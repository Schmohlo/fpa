package com.us.ifp.fpa.flightplanstuff;

/**
 * Created by V582 on 13.12.2016.
 */


import android.app.Activity;
import android.widget.Toast;

import com.us.ifp.fpa.MainActivity;
import com.us.ifp.fpa.MissionControl.MCLogger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import dji.common.error.DJIError;
import dji.common.gimbal.DJIGimbalAngleRotation;
import dji.common.gimbal.DJIGimbalRotateAngleMode;
import dji.common.gimbal.DJIGimbalRotateDirection;
import dji.common.util.DJICommonCallbacks;
import dji.sdk.missionmanager.DJICustomMission;
import dji.sdk.missionmanager.DJIWaypoint;
import dji.sdk.missionmanager.DJIWaypointMission;
import dji.sdk.missionmanager.missionstep.DJIAircraftYawStep;
import dji.sdk.missionmanager.missionstep.DJIGimbalAttitudeStep;
import dji.sdk.missionmanager.missionstep.DJIGoToStep;
import dji.sdk.missionmanager.missionstep.DJIMissionStep;
import dji.sdk.missionmanager.missionstep.DJIShootPhotoStep;
import dji.sdk.missionmanager.missionstep.DJITakeoffStep;
import dji.sdk.missionmanager.missionstep.DJIWaypointStep;

import static java.lang.Math.max;
import static java.lang.Math.signum;
import static java.lang.Math.sqrt;


public class FlightPlan {

    // fields for saving data and meta-data in object:
    private long         id;
    private int  		 numberOfFinishedSteps;
    private String  	 dateOfLoad;
    private String  	 dateOfLastChange;
    public  Properties   properties;
    private List<String> lineDataSets;


    // constant fields for parsing:
    public static final Charset CHARSET           = Charset.forName("utf-8");
    public static final String  HEADER_TAG        = "header";
    public static final String  DATA_TAG          = "data";
    public static final String  START_COMMENT_TAG = "<!--";
    public static final String    END_COMMENT_TAG = "-->";
    public static final String  DATA_LINE_REGEXPR =    "-?[0-9]{1,3}\\.[0-9]+" + // latitude
                                                    "\\s+-?[0-9]{1,3}\\.[0-9]+" + // longitude
                                                    "\\s+-?[0-9]{1,3}\\.[0-9]+" + // height
                                                    "\\s+-?[0-9]{1,3}\\.[0-9]+" + // heading/hea
                                                    "\\s+-?[0-9]{1,3}\\.[0-9]+" + // camera pitch
                                                    "\\s+[0-1]"; // take Picture yes/no

    // constant fields for range validations:
    public static final double[] RANGE_LAT = { -90, 90};
    public static final double[] RANGE_LON = {-180,180};
    public static final double[] RANGE_YAW = {-180,180};
    public static final double[] RANGE_PIT = { -90,  0};

    // constant fields:
    public static final double CENTRALPOINT_DIFF_THRESHOLD = 1.0E-9;
    public static final double CENTRALPOINT_DIFF_THRESHOLD_DEGREE = 1.0E-9;
    public static final double CENTRALPOINT_DIFF_THRESHOLD_HEIGHT = 1.0E-3;


    // Constructor
    public FlightPlan() {
        // Initiate fields:
        id                    = -1;
        numberOfFinishedSteps = 0;
        dateOfLoad            = null;
        dateOfLastChange      = calendarToString(Calendar.getInstance()); // Set dateOfLastChange to current date
        properties            = null;
        lineDataSets          = null;
    }


    /** Constructor
     * Warning: there is no check for number of points/pictures or central point because this
     * constructor is to be used only when reading from database. And sets in the database have
     * already been approved.
     */
    public FlightPlan(Long id, int numberOfFinishedSteps, String dateOfLoad, String dateOfLastChange,
            String name, String dateOfPlanning, String centralPoint, String numberOfWaypoints,
            String numberOfWayPointsWithPictures, String description, String lineDataSets) {

        // Initiate fields:
        this.id                    = id;
        this.numberOfFinishedSteps = numberOfFinishedSteps;
        this.dateOfLoad            = dateOfLoad;
        this.dateOfLastChange      = dateOfLastChange;

        this.properties = new Properties();
        this.properties.setPropertyByName("name", name);
        this.properties.setPropertyByName("dateOfPlanning", dateOfPlanning);
        this.properties.setPropertyByName("centralPoint", centralPoint);
        this.properties.setPropertyByName("numberOfWaypoints", numberOfWaypoints);
        this.properties.setPropertyByName("numberOfWayPointsWithPictures", numberOfWayPointsWithPictures);
        this.properties.setPropertyByName("description", description);

        String[] lines = lineDataSets.split("\n");
        this.lineDataSets = new ArrayList<String> (Arrays.asList(lines));
    }



    public static String calendarToString(Calendar calendar) {
        // 2016-12-06-13:37
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd-HH:mm");
        return format1.format(calendar.getTime());
    }


    // Class for properties initialized by file:
    public static  class Properties {
        public static final String[] names = {
                "name",
                "dateOfPlanning",
                "centralPoint",
                "numberOfWaypoints",
                "numberOfWayPointsWithPictures",
                "description"};
        public static final String[] expressions = {
                "name:\\s*.+",
                "date:\\s*[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}-[0-9]{1,2}:[0-9]{1,2}",
                "CentralPoint:\\s*[0-9]{1,3}\\.[0-9]+\\s+[0-9]{1,3}\\.[0-9]+\\s+[0-9]+\\.[0-9]+",
                "NofPoints:\\s*[0-9]+",
                "NofPictures:\\s*[0-9]+",
                "description:\\s*.+"};
        private String[] values = {"","","","","",""};

        // setter an getter methods:
        public boolean setPropertyByName(String name, String value) throws IllegalArgumentException {

            if(!Arrays.asList(names).contains(name) ) {
                throw new IllegalArgumentException(String.format("Invalid property name \"%s\".", name));
            } else {
                int index = Arrays.asList(names).indexOf(name);
                String expr = expressions[index];
                String expr2 = expr.split(":", 2)[1];

                //
                if(Pattern.matches(expr2, value) ) {
                    values[index] = value.trim();
                    return true;
                } else
                    return false;
            }
        }

        public String getPropertyByNameAsString(String name) throws IllegalArgumentException {

            if(!Arrays.asList(names).contains(name)) {
                throw new IllegalArgumentException(String.format("Invalid property name \"%s\".", name));
            } else {
                int index = Arrays.asList(names).indexOf(name);
                return values[index];
            }
        }
        public Calendar getPropertyByNameAsCalendar(String name) throws IllegalArgumentException {

            if(!Arrays.asList(names).contains(name) || name.length() < 4 || !name.substring(0,4).equals("date")) {
                throw new IllegalArgumentException(String.format("Invalid property name \"%s\".", name));
            } else {
                int index = Arrays.asList(names).indexOf(name);
                String date = values[index];

                String[] parts = date.split("-");
                String[] time  = parts[3].split(":");

                Calendar calendar = Calendar.getInstance();
                calendar.clear();
                calendar.set(
                        Integer.parseInt(parts[0]),   // year
                        Integer.parseInt(parts[1])-1, // month
                        Integer.parseInt(parts[2]));  // day
                calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time[0]));
                calendar.set(Calendar.MINUTE, Integer.parseInt(time[1]));

                return calendar;
            }
        }
        private double[] getPropertyByNameAsPoint(String name) throws IllegalArgumentException {

            if(!Arrays.asList(names).contains(name) || name.length() < 5 || !name.substring(name.length()-5).equals("Point")) {
                throw new IllegalArgumentException(String.format("Invalid property name \"%s\".", name));
            } else {
                int index = Arrays.asList(names).indexOf(name);
                String point = values[index];
                point = point.replaceAll("\\s+", " ");
                String[] parts = point.split("\\s");

                return new double[] {
                        Double.parseDouble(parts[0]), // latitude
                        Double.parseDouble(parts[1]), // longitude
                        Double.parseDouble(parts[2])};// height
            }
        }
    }



    public static class ReadReport {
        public final String   name;
        public final String   dateOfPlanning;
        public final int      numberOfWaypoints;
        public final int      numberOfWayPointsWithPictures;
        public final double[] centralPoint;
        public final int      numberOfWaypointsT;              // T for theoretically
        public final int      numberOfWayPointsWithPicturesT;  // T for theoretically
        public final double[] centralPointT;                   // T for theoretically
        public final double[] centralPointD;                   // D for difference
        public final boolean  warnings;
        public final boolean  errors;
        public final String   checkMessage;

        public ReadReport(String name, String dateOfPlanning, int numberOfWaypoints, int numberOfWayPointsWithPictures, double[] centralPoint, int numberOfWaypointsT, int numberOfWayPointsWithPicturesT, double[] centralPointT) {

            if(centralPoint.length < 3 || centralPointT.length < 3 )
                throw new IllegalArgumentException(String.format("numericCheck must have at least 3 elements."));
            this.name                           = name;
            this.dateOfPlanning                 = dateOfPlanning;
            this.numberOfWaypoints              = numberOfWaypoints;
            this.numberOfWayPointsWithPictures  = numberOfWayPointsWithPictures;
            this.centralPoint                   = centralPoint;
            this.numberOfWaypointsT             = numberOfWaypointsT;
            this.numberOfWayPointsWithPicturesT = numberOfWayPointsWithPicturesT;
            this.centralPointT                  = centralPointT;

            centralPointD = new double[3];
            for(int i=0;i<3;i++) {
                centralPointD[i] = centralPoint[i] - centralPointT[i];
            }

            this.warnings = !(Math.abs(centralPointD[0]) < CENTRALPOINT_DIFF_THRESHOLD_DEGREE &&
                       Math.abs(centralPointD[1]) < CENTRALPOINT_DIFF_THRESHOLD_DEGREE &&
                       Math.abs(centralPointD[2]) < CENTRALPOINT_DIFF_THRESHOLD_HEIGHT);
            this.errors = !(numberOfWaypoints == numberOfWaypointsT &&
                    numberOfWayPointsWithPictures == numberOfWayPointsWithPicturesT );

            String lMessage = !(this.warnings || this.errors) ? "" : "There was a mismatch in check values! \n";
            lMessage += "Please check data below: \n";
            lMessage += String.format("#Waypoints: %1$d - %2$d  / ", this.numberOfWaypoints, this.numberOfWaypointsT);
            lMessage += this.numberOfWaypoints == this.numberOfWaypointsT ? "ok" : "error";
            lMessage += String.format("\n#Pictures: %1$d - %2$d  / ", this.numberOfWayPointsWithPictures, this.numberOfWayPointsWithPicturesT);
            lMessage += this.numberOfWayPointsWithPictures == this.numberOfWayPointsWithPicturesT ? "ok" : "error";
            lMessage += String.format("\nCP Lat: diff = %1$6.3e°  / ", this.centralPointD[0]);
            lMessage += Math.abs(this.centralPointD[0]) < FlightPlan.CENTRALPOINT_DIFF_THRESHOLD_DEGREE ? "ok" : "warning";
            lMessage += String.format("\nCP Long: diff = %1$6.3e°  / ", this.centralPointD[1]);
            lMessage += Math.abs(this.centralPointD[1]) < FlightPlan.CENTRALPOINT_DIFF_THRESHOLD_DEGREE ? "ok" : "warning";
            lMessage += String.format("\nCP Height: diff = %1$6.3em  / ", this.centralPointD[2]);
            lMessage += Math.abs(this.centralPointD[2]) < FlightPlan.CENTRALPOINT_DIFF_THRESHOLD_HEIGHT ? "ok" : "warning";
            checkMessage = lMessage;
        }
    }



    /** Initialize FlightPlan by reading a file.
     *  If there accours an error and an exception is thrown,
     *  this.dateOfLoad, this.properties and this.lineDataSet
     *  will ne null!
     */
    public ReadReport readFromFile(InputStream inputStream) throws FlightPlanParseException {

        boolean isInComment = false;
        boolean isInHeader  = false;
        boolean isInData    = false;
        boolean hasFinishedHeader = false; // to improve performance, e.g. don't have to search for header-start, if header has allready been read.
        boolean hasFinishedData   = false; // to improve performance


		/*
		 * temporary fields. Set the fields of this Object only when successful,
		 * that means when no exception have been thrown.
		 */
        Properties   tempProperties   = new Properties();
        List<String> tempLineDataSets = new ArrayList<String>();

        double controlSumLat = 0;
        double controlSumLon = 0;
        double controlSumHei = 0;
        int    controlSumPoi = 0;
        int    controlSumPic = 0;



        try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            String line = null;
            int lineCounter = 0;

            while ((line = reader.readLine()) != null) {

                line = line.trim();
                lineCounter++;

                // jump over blank lines
                if(line.isEmpty()) {
                    continue;
                }

                // throw out comments:
                if(!isInComment) {
                    if(line.contains(START_COMMENT_TAG)) {
                        isInComment = true;
                        // if it is a one-line comment:
                        if(line.contains(END_COMMENT_TAG)) {
                            isInComment = false;
                        }
                        continue;
                    }
                } else {
                    // ability to have multiple lines
                    if(line.contains(END_COMMENT_TAG)) {
                        isInComment = false;
                        continue;
                    }
                }

                // search for header-tags
                if(!hasFinishedHeader && !isInHeader) {
                    if(line.contains(makeStartTag(HEADER_TAG))) {
                        isInHeader = true;
                        continue;
                    }
                }
                if(isInHeader && !hasFinishedHeader) {
                    if(line.contains(makeEndTag(HEADER_TAG))) {
                        isInHeader = false;
                        hasFinishedHeader = true;

                        // TODO: change this as well when adjusting the properties!
                        // check, if all necessary properties have been set!
                        for(int i=0; i<Properties.names.length; i++) {
                            // first check if there is a valid attribute name
                            if(tempProperties.values[i].equals("") && !tempProperties.names[i].equals("description")) {
                                throw new FlightPlanParseException("Unable to read " + Properties.names[i] +
                                        ". Format should be \"" +
                                        Properties.expressions[i]  +"\".");
                            }
                        }

                        continue;
                    }
                }

                // search for data-tags
                if(!hasFinishedData &&  !isInData) {
                    if(line.contains(makeStartTag(DATA_TAG))) {
                        isInData = true;
                        continue;
                    }
                }
                if(isInData && !hasFinishedData) {
                    if(line.contains(makeEndTag(DATA_TAG))) {
                        isInData = false;
                        hasFinishedData = true;
                        continue;
                    }
                }


                // Reading of header infos:
                if(isInHeader) {

                    // valid attribute line? can check here basic for performance
                    if(!line.contains(":"))
                        throw new FlightPlanParseException(String.format(Locale.GERMAN,"Invalid header entry at line %1d.", lineCounter));

                    // check for all the possible properties and set them
                    boolean foundValidProp = false;
                    for(int i=0; i<Properties.names.length; i++) {

                        // check if the statement including the value is valid
                        if(Pattern.matches(Properties.expressions[i], line)) {

                            if(tempProperties.values[i].equals("")) {
                                tempProperties.setPropertyByName(Properties.names[i], line.split(":", 2)[1]);
                            } else {
                                throw new FlightPlanParseException(String.format("Header-info %s appears to declared more than once.",Properties.names[i]));
                            }

                            foundValidProp = true;
                            break;
                        }
                    }
                    if(foundValidProp) continue;

                    // finally, now the line is invalid:
                    throw new FlightPlanParseException(String.format(Locale.GERMAN,"Invalid header entry at line %1d.", lineCounter));
                }



                if(isInData) {
                    // get the data extracted from line String and do validation on the fly:
                    LineData lineData = extractLineData(line,lineCounter);

                    // Update control sums:
                    controlSumLat += lineData.lat;
                    controlSumLon += lineData.lon;
                    controlSumHei += lineData.hei;
                    controlSumPoi ++;
                    controlSumPic += lineData.pic;

                    // Update internal data sets storage:
                    tempLineDataSets.add(line);
                }
            } // End while loop for all lines


            // Throw exception if there are was no closing data and header tag:
            if(!hasFinishedHeader)
                throw new FlightPlanParseException(String.format(Locale.GERMAN,"Header tag had not been closed"));
            if(!hasFinishedData)
                throw new FlightPlanParseException(String.format(Locale.GERMAN,"Data tag had not been closed"));


        } catch (IOException x) {
            throw new FlightPlanParseException(String.format("IOException: %s", x.getMessage()));
        }


        // save data in this object:
        this.numberOfFinishedSteps = 0;
        this.dateOfLoad            = calendarToString(Calendar.getInstance());
        this.dateOfLastChange      = calendarToString(Calendar.getInstance());
        this.properties            = tempProperties;
        this.lineDataSets          = tempLineDataSets;

        // check sums to central point:
        double[] checkpoint = controlSumPoi != 0 ? new double[] {controlSumLat / controlSumPoi,
                                                                 controlSumLon / controlSumPoi,
                                                                 controlSumHei / controlSumPoi}
                                                 : new double[] {0,0,0};

        // return report:
        return new ReadReport(tempProperties.getPropertyByNameAsString("name"),
                tempProperties.getPropertyByNameAsString("dateOfPlanning"),
                controlSumPoi, controlSumPic, checkpoint,
                Integer.parseInt(tempProperties.getPropertyByNameAsString("numberOfWaypoints")),
                Integer.parseInt(tempProperties.getPropertyByNameAsString("numberOfWayPointsWithPictures")),
                tempProperties.getPropertyByNameAsPoint("centralPoint"));
    }




    private static class LineData {
        public double lat; // latitude
        public double lon; // longitude
        public double hei; // GPS height
        public double hea; // heading
        public double pit; // pitch
        public int    pic; // picture yes/no
        public LineData() {}
    }




    private static LineData extractLineData(String line, int lineCounter) throws FlightPlanParseException {

        if(Pattern.matches(DATA_LINE_REGEXPR, line)) {

            line = line.replaceAll("\\s+", " ");
            String[] dataParts = line.split("\\s");
            LineData lineData = new LineData();
            lineData.lat = Double.parseDouble(dataParts[0]);
            lineData.lon = Double.parseDouble(dataParts[1]);
            lineData.hei = Double.parseDouble(dataParts[2]);
            lineData.hea = Double.parseDouble(dataParts[3]);
            lineData.pit = Double.parseDouble(dataParts[4]);
            lineData.pic = Integer.parseInt(  dataParts[5]);

            // test whether data sets are valid.
            if(lineData.lat < RANGE_LAT[0] || lineData.lat > RANGE_LAT[1])
                throw new FlightPlanParseException(String.format(Locale.GERMAN,"Invalid Latitude Range on line %d.",lineCounter));
            if(lineData.lon < RANGE_LON[0] || lineData.lon > RANGE_LON[1])
                throw new FlightPlanParseException(String.format(Locale.GERMAN,"Invalid Longitude Range on line %d.",lineCounter));
            if(lineData.hea < RANGE_YAW[0] || lineData.hea > RANGE_YAW[1])
                throw new FlightPlanParseException(String.format(Locale.GERMAN,"Invalid Heading Range on line %d.",lineCounter));
            if(lineData.pit < RANGE_PIT[0] || lineData.pit > RANGE_PIT[1])
                throw new FlightPlanParseException(String.format(Locale.GERMAN,"Invalid CameraPitch Range on line %d.",lineCounter));

            return lineData;

        } else {
            throw new FlightPlanParseException(String.format(Locale.GERMAN,"Illegal data set on line %d. Format should be \"%s\".", lineCounter,DATA_LINE_REGEXPR));
        }
    }



    /** generateCustomMission(...) is not going to be maintained, because it is
     *  a) not very convenient
     *  b) does not work yet (bugs)
     *  c) IMPORTANT: goTo points have to have a distance of about 40cm. WaypointMissions have
     *     a security mechanism that prevents this (2m min. distance). So WaypointMissions are
     *     the thing to go with here
     */
    public DJICustomMission generateCustonMission(Activity activity, int startPoint, int numberOfPoints, double takeOffHeight, double goHomeAltitude ) {


        //
        // TO_DO: heading funktioniert nicht richtig.

        List<DJIMissionStep> steps = new ArrayList<DJIMissionStep>();


        // alle  callbacks sinnvoll ausfüllen?
        // Problem: brauche instanz der activity oder control framgents

        final MainActivity mainActivity;

        if(activity instanceof MainActivity) {
            mainActivity = (MainActivity) activity;
        } else {
            mainActivity = null;
        }


        // start with a take off Step:
        steps.add(new DJITakeoffStep(new DJICommonCallbacks.DJICompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                if(djiError != null && mainActivity != null) {
                    mainActivity.showToast(djiError.getDescription(), Toast.LENGTH_LONG, "TakeoffStep", MCLogger.LogType.e);
                }
            }}));


        // go to goHome Altitude (from startpoint):
        steps.add(new DJIGoToStep((float) goHomeAltitude,new DJICommonCallbacks.DJICompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                if(djiError != null && mainActivity != null) {
                    mainActivity.showToast(djiError.getDescription(), Toast.LENGTH_LONG, "GoToStep", MCLogger.LogType.e);
                }
            }}));


//        // go to first point in goHme Altitude:
        LineData data = null;
        try {
            data = extractLineData(lineDataSets.get(startPoint -1), 0);
        } catch (Exception e) {
            // this should never be called, because the lines have already been validated!
            // if this gets called there is a serious problem!
        }

        // go to first point in goHome altitude or at least 3m above first point. And then to first point.
        // this will be done in a wayPoint mission so that heading is set.
        DJIWaypointMission WPmission = new DJIWaypointMission();

        // point above first point:
        float altitude = (float)(data.hei-takeOffHeight) + 3;
        if((float) goHomeAltitude > altitude) {
            altitude = (float) goHomeAltitude;
        }
        DJIWaypoint wayPoint = new DJIWaypoint(data.lat,data.lon,altitude);
        wayPoint.heading     = (short) data.hea;
        WPmission.addWaypoint(wayPoint);

        // first point:
        wayPoint = new DJIWaypoint(data.lat,data.lon,(float) (data.hei-takeOffHeight));
        wayPoint.heading     = (short) data.hea;
        wayPoint.gimbalPitch = (float) data.pit;
        if( data.pic == 1) {
            wayPoint.hasAction = wayPoint.addAction(new DJIWaypoint.DJIWaypointAction(DJIWaypoint.DJIWaypointActionType.StartTakePhoto, 0));
        }
        WPmission.addWaypoint(wayPoint);

        // ad to mission:
        steps.add(new DJIWaypointStep(WPmission, new DJICommonCallbacks.DJICompletionCallback(){
            @Override
            public void onResult(DJIError djiError) {
                if(djiError != null && mainActivity != null) {
                    mainActivity.showToast(djiError.getDescription(), Toast.LENGTH_LONG, "WaypointStep", MCLogger.LogType.e);
                }
            }
        }));

        double lastHeading = data.hea;
        double yaw = 0;
        // loop over all points that will go into the mission (except first one, see above).
        // I put evey point in a single wayPoint mission. For conventience in programming
        // and I hope in later performance, because this will be send in one block to drone.
        for(int i=1; i < Math.min(numberOfPoints,this.lineDataSets.size()-startPoint+1); i++) {

//            steps.add(new DJIWaypointStep(generateWaypointMission(startPoint+i,1,takeOffHeight,goHomeAltitude),
//                    new DJICommonCallbacks.DJICompletionCallback() {
//                        @Override
//                        public void onResult(DJIError djiError) {
//
//                        }
//                    }));
            data = null;
            try {
                data = extractLineData(lineDataSets.get(startPoint + i-1), 0);
            } catch (Exception e) {
                // this should never be called, because the lines have already been validated!
                // if this gets called there is some serious problem!
            }
            yaw        = lastHeading - data.hea; // TODO: muss yaw auf 0..360 gebracht werden? sind negative Werte ok?
            lastHeading = data.hea;

            // add new Waypoint in  manuel steps:
            steps.add(new DJIGoToStep(data.lat, data.lon, (float) (data.hei-takeOffHeight), new DJICommonCallbacks.DJICompletionCallback() {
                @Override
                public void onResult(DJIError djiError) {
                    if(djiError != null && mainActivity != null) {
                        mainActivity.showToast(djiError.getDescription(), Toast.LENGTH_LONG, "GoToStep",MCLogger.LogType.e);
                    }
                }
            }));
            steps.add(new DJIAircraftYawStep(yaw, 20, new DJICommonCallbacks.DJICompletionCallback() {
                @Override
                public void onResult(DJIError djiError) {
                    if(djiError != null && mainActivity != null) {
                        mainActivity.showToast(djiError.getDescription(), Toast.LENGTH_LONG, "YawStep",MCLogger.LogType.e);
                    }
                }
            }));
            steps.add(new DJIGimbalAttitudeStep(
                    DJIGimbalRotateAngleMode.AbsoluteAngle,
                    new DJIGimbalAngleRotation(true, Math.abs((float) data.pit), DJIGimbalRotateDirection.Clockwise),
                    new DJIGimbalAngleRotation(true, 0, DJIGimbalRotateDirection.Clockwise),
                    new DJIGimbalAngleRotation(true, 0, DJIGimbalRotateDirection.Clockwise),
                    new DJICommonCallbacks.DJICompletionCallback() {
                        @Override
                        public void onResult(DJIError djiError) {
                            if(djiError != null && mainActivity != null) {
                                mainActivity.showToast(djiError.getDescription(), Toast.LENGTH_LONG, "GimbalAttitudeStep",MCLogger.LogType.e);
                            }
                        }
                    }
            ));
            if(data.pic == 1) {
                steps.add(new DJIShootPhotoStep(new DJICommonCallbacks.DJICompletionCallback() {
                    @Override
                    public void onResult(DJIError djiError) {
                        if (djiError != null && mainActivity != null) {
                            mainActivity.showToast(djiError.getDescription(), Toast.LENGTH_LONG, "ShootPhotoStep",MCLogger.LogType.e);
                        }
                    }
                }));
            }

        } // End for loop.

        // go to goHome Altitude (from endpoint):
        steps.add(new DJIGoToStep((float) goHomeAltitude,new DJICommonCallbacks.DJICompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                if (djiError != null && mainActivity != null) {
                    mainActivity.showToast(djiError.getDescription(), Toast.LENGTH_LONG, "DJIGoToStep", MCLogger.LogType.e);
                }
            }
        }));


        DJICustomMission mission = new DJICustomMission(steps);

        return null;
    }




    public DJIWaypointMission genereateWaypointMissionWithGoHomeAltitudeAndStartEndHoverOverHomePlus3m(double HPlat, double HPlon, int startPoint, int numberOfPoints, double takeOffHeight, double goHomeAltitude, double latitudeOffset, double longitudeOffset ) {


        float AltitudeAddition = 2.1f; // [m]


        // get the waypointlist of "pure" mission:
        DJIWaypointMission mission1    = genereateWaypointMissionWithGoHomeAltitude(startPoint, numberOfPoints, takeOffHeight, goHomeAltitude, latitudeOffset, longitudeOffset );
        List<DJIWaypoint> waypointList = mission1.waypointsList;


        // new mission:
        DJIWaypointMission mission = new DJIWaypointMission();


        // new points :
        if( !waypointList.isEmpty() ) {

            // to make sure, that the 2m min. distance is kept, add additional 2.1m in Altitude.
            // Don't make a mission not fixed with +2 mission +2, because other code relates on that
            // pattern!

            // start point:
            DJIWaypoint waypoint = new DJIWaypoint(HPlat, HPlon, waypointList.get(0).altitude + AltitudeAddition);
            waypointList.add(0, waypoint);
            // end hovering point:
            waypoint = new DJIWaypoint(HPlat, HPlon, waypointList.get(waypointList.size()-1).altitude + AltitudeAddition);
            waypointList.add(waypointList.size(), waypoint);
        }

        mission.addWaypoints(waypointList);

        // Required (also for Phantom 4):
        mission.headingMode = DJIWaypointMission.DJIWaypointMissionHeadingMode.UsingWaypointHeading;
        // Required for the gimbal to able to change pitch:
        mission.needRotateGimbalPitch = true;

        return mission;

    }



    // this will add waypoints at the beginning and the end, which are on the same location, but
    // at "go home altitude"
    private DJIWaypointMission genereateWaypointMissionWithGoHomeAltitude(int startPoint, int numberOfPoints, double takeOffHeight, double goHomeAltitude, double latitudeOffset, double longitudeOffset ) {


        float AltitudeAddition = 2.1f; // [m] // to make sure the 2m distance is kept


        // get the waypointlist of "pure" mission:
        DJIWaypointMission mission1    = generateWaypointMission(startPoint, numberOfPoints, takeOffHeight, latitudeOffset, longitudeOffset );
        List<DJIWaypoint> waypointList = mission1.waypointsList;


        // new mission:
        DJIWaypointMission mission = new DJIWaypointMission();

        // new points:
        if( !waypointList.isEmpty()) {
            // start point:
            DJIWaypoint waypoint = new DJIWaypoint(
                    waypointList.get(0).latitude,
                    waypointList.get(0).longitude,
                    max((float) goHomeAltitude, waypointList.get(0).altitude + AltitudeAddition));
            // start point hase same heading as first "real" point, so that the drone has no problem
            // with the low rotating rate.
            waypoint.heading     =  waypointList.get(0).heading;

            waypointList.add(0, waypoint);


            // end point:
            waypoint = new DJIWaypoint(
                    waypointList.get(waypointList.size() - 1).latitude,
                    waypointList.get(waypointList.size() - 1).longitude,
                    max((float) goHomeAltitude, waypointList.get(waypointList.size()-1).altitude + AltitudeAddition));
            waypointList.add(waypointList.size(), waypoint);
        }

        mission.addWaypoints(waypointList);

        // Required (also for Phantom 4):
        mission.headingMode = DJIWaypointMission.DJIWaypointMissionHeadingMode.UsingWaypointHeading;
        // Required for the gimbal to able to change pitch:
        mission.needRotateGimbalPitch = true;

        return mission;
    }





    private DJIWaypointMission generateWaypointMission(int startPoint, int numberOfPoints, double takeOffHeight, double latitudeOffset, double longitudeOffset) {

        DJIWaypointMission mission = new DJIWaypointMission();

        double nextheading = 0;
        double ch360  = 0;
        double nh360  = 0;

        // loop over all points that will go into the mission:
        for(int i=0; i < Math.min(numberOfPoints,this.lineDataSets.size()-startPoint+1); i++) {

            LineData data = null;
            try {
                data = extractLineData(lineDataSets.get(startPoint + i-1), 0);
                if( i < Math.min(numberOfPoints,this.lineDataSets.size()-startPoint+1)) {
                    LineData nextdata = extractLineData(lineDataSets.get(startPoint + i), 0);
                    nextheading = nextdata.hea;
                }
            } catch (Exception e) {
                // this should never be called, because the lines have already been validated!
                // if this gets called there is some serious problem!
                // TODO: valid handling!
            }


            // add new Waypoint:
            DJIWaypoint waypoint = new DJIWaypoint(data.lat + latitudeOffset,data.lon + longitudeOffset,(float)(data.hei-takeOffHeight));
            waypoint.heading     = (short) data.hea;


            // workaround for that rotating in the shorter direction, because:
            // 1.) faster rotation.
            // 2.) so that the drone has enough time between two points to rotate, so that pictures
            //     are taken at correct heading.
            if(data.hea    < 0) ch360 = data.hea    + 360; else ch360 = data.hea;
            if(nextheading < 0) nh360 = nextheading + 360; else nh360 = nextheading;

            if( (nh360 - ch360) > signum(nh360-ch360)*180 ) {
                waypoint.turnMode = DJIWaypoint.DJIWaypointTurnMode.CounterClockwise;
            } else {
                waypoint.turnMode = DJIWaypoint.DJIWaypointTurnMode.Clockwise;
            }


            waypoint.gimbalPitch = (float) data.pit;
            if( data.pic == 1) {
                waypoint.hasAction = waypoint.addAction(new DJIWaypoint.DJIWaypointAction(DJIWaypoint.DJIWaypointActionType.StartTakePhoto, 0));
            }
            mission.addWaypoint(waypoint);
        }

        // Required (also for Phantom 4):
        mission.headingMode = DJIWaypointMission.DJIWaypointMissionHeadingMode.UsingWaypointHeading;
        // Required for the gimbal to able to change pitch:
        mission.needRotateGimbalPitch = true;

        return mission;
    }




    private static String makeStartTag(String tagName) {
        return "<" + tagName + ">";
    }
    private static String makeEndTag(String tagName) {
        return "</" + tagName + ">";
    }






    /** Setter Methods
     *
     */
    public void setNumberOfFinishedSteps(int numberOfFinishedSteps) {

        this.numberOfFinishedSteps = numberOfFinishedSteps;
        this.dateOfLastChange = calendarToString(Calendar.getInstance());
    }

    /** Getter Methods
     *
     */
    public int getNumberOfFinishedSteps() {
        return numberOfFinishedSteps;
    }
    public long getID() {
        return id;
    }
    public String getDateOfLoad() {
        return dateOfLoad;
    }
    public String getDateOfLastChange() {
        return dateOfLastChange;
    }


    public List<String> getLineDataSets() {
        return lineDataSets;
    }

}