package com.us.ifp.fpa.MissionControl;

import android.util.Log;

import java.io.IOException;
import java.util.Calendar;
import java.util.Locale;

import dji.common.flightcontroller.DJIGPSSignalStatus;

/**
 * Created by V582 on 27.02.2017.
 */

public class MCLogger extends  FPALogger{

    // Constant strings:
    private static final String LOG_TAG = MCLogger.class.getName();

    private static final String FILENAME_SUFFIX = "MissionControl";
    private static final String HEADLINE        = "Timestamp, latitude[°], longitude[°], altitude[m], heading[°], pitch[°], GPSSignal, isflying, LogType: Message";


    // format Strings:
    private static final String LOG_ENTRY_POSITION_FORMAT    = " %12.7f,%12.7f,%7.2f,%7.2f,%7.2f";
    private static final String LOG_ENTRY_INFORMATION_FORMAT = " GPS%s, %3s flying";
    private static final String LOG_ENTRY_MESSAGE_FORMAT     = " %s: %s";
    private static final String LOG_ENTRY_COMBINED_FORMAT    = ", " + LOG_ENTRY_POSITION_FORMAT +
                                                               ", " + LOG_ENTRY_INFORMATION_FORMAT +
                                                               ", " + LOG_ENTRY_MESSAGE_FORMAT;

    private static final String LOG_ENTRY_COMBINED_FORMAT_WITH_TIME = DATE_FORMAT.toPattern() + LOG_ENTRY_COMBINED_FORMAT;


    public enum LogType {
        i,w,e
    }



    public MCLogger() {
        super(FILENAME_SUFFIX);
    }

    @Override
    public void log(String logEntry) throws IOException {
        try {
            writeToLogFile(logEntry);
        } catch (IOException e) {
            throw e;
        }
    }

    @Override
    protected void writeHeadLine() throws IOException{
        String headerLines = HEADLINE +NEWLINE + LOG_ENTRY_COMBINED_FORMAT_WITH_TIME + NEWLINE;
        writeToLogFile(headerLines);
    }



    public void log(String message, LogType type, double lat, double lon, float altitude, double heading, float pitch, boolean isflying, DJIGPSSignalStatus djigpsSignalStatus) throws IOException {

        // assemble log string:
        // timestamp:
        String timeString  = DATE_FORMAT.format(Calendar.getInstance().getTime());
        // final assemble:
        // use us locale for "." as decimal seperator
        String logEntry = String.format(Locale.US,"%s" + LOG_ENTRY_COMBINED_FORMAT + NEWLINE,
                timeString,lat,lon,altitude,heading,pitch,djigpsSignalStatus.toString(),(isflying? "is":"not"),type.toString(),message);

        try {
            log(logEntry);
            switch (type) {
                case i:
                    Log.i(LOG_TAG, logEntry);
                    break;
                case w:
                    Log.w(LOG_TAG, logEntry);
                    break;
                case e:
                    Log.e(LOG_TAG, logEntry);
                    break;
            }
        } catch (IOException e) {
            throw e;
        }
    }


}
