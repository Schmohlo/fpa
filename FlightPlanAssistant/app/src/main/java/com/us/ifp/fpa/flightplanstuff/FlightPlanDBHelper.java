package com.us.ifp.fpa.flightplanstuff;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by V582 on 20.12.2016.
 */


public class FlightPlanDBHelper extends SQLiteOpenHelper {

    private static final String TAG = FlightPlanDBHelper.class.getName();


    public static final String DB_NAME = "flightPlans.db";
    public static final int DB_VERSION = 2;

    public static final String TABLE_FLIGHTPLANS = "flightPlans";

    public static final String COLUMN_ID                            = "_id";
    public static final String COLUMN_NAME                          = "name";
    public static final String COLUMN_DATEOFPLANNING                = "dateOfPlanning";
    public static final String COLUMN_CENTRALPOINT                  = "centralPoint";
    public static final String COLUMN_NUMBEROFWAYPOINTS             = "numberOfWaypoints";
    public static final String COLUMN_NUMBEROFWAYPOINTSWITHPICTURES = "numberOfWayPointsWithPictures";
    public static final String COLUMN_DESCRIPTION                   = "description";
    public static final String COLUMN_NUMBEROFFINISHEDSTEPS         = "numberOfFinishedSteps";
    public static final String COLUMN_DATEOFLOAD                    = "dateOfLoad";
    public static final String COLUMN_DATEOFLASTCHANGE              = "dateOfLastChange";
    public static final String COLUMN_lINEDATASETS                  = "lineDataSets";




    public static final String SQL_CREATE =
            "CREATE TABLE " + TABLE_FLIGHTPLANS +
                    "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_NAME                           + " TEXT NOT NULL, " +
                    COLUMN_DATEOFPLANNING                 + " TEXT NOT NULL, " +
                    COLUMN_CENTRALPOINT                   + " TEXT NOT NULL, " +
                    COLUMN_NUMBEROFWAYPOINTS              + " TEXT NOT NULL, " +
                    COLUMN_NUMBEROFWAYPOINTSWITHPICTURES  + " TEXT NOT NULL, " +
                    COLUMN_DESCRIPTION                    + " TEXT NOT NULL, " +
                    COLUMN_NUMBEROFFINISHEDSTEPS          + " INTEGER NOT NULL, " +
                    COLUMN_DATEOFLOAD                     + " TEXT NOT NULL, " +
                    COLUMN_DATEOFLASTCHANGE               + " TEXT NOT NULL, " +
                    COLUMN_lINEDATASETS                   + " TEXT NOT NULL);";




    public FlightPlanDBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        Log.d(TAG, "DbHelper hat die Datenbank: " + getDatabaseName() + " v" + DB_VERSION + " erzeugt.");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            Log.d(TAG, "Die Tabelle wird mit SQL-Befehl: " + SQL_CREATE + " angelegt.");
            db.execSQL(SQL_CREATE);
        }
        catch (Exception ex) {
            Log.e(TAG, "Fehler beim Anlegen der Tabelle: " + ex.getMessage());
        }


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}