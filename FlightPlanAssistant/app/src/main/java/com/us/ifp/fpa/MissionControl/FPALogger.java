package com.us.ifp.fpa.MissionControl;

/**
 * Created by V582 on 03.03.2017.
 */

import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


public abstract class FPALogger {

    // TODO: good exception handling!!!!!!!!!!!


    // Constant strings:
    private static final String LOG_TAG = FPALogger.class.getName();

    protected static final String LOG_FILE_DIR       = "FlightPlanAssistant" + File.separator + "LogFiles";
    protected static final String FILENAME_PREFIX    = "FPA_LogFile";
    protected static final String FILENAME_TYPE      = ".txt";

    protected static final String NEWLINE            = "\r\n";
    protected static final String HEADLINE_SEPARATOR = "---"+NEWLINE;


    // format Strings:
    protected static final SimpleDateFormat DATE_FORMAT           = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    protected static final SimpleDateFormat DATE_FORMAT_FILE_NAME = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");

    protected static final String FILENAME_FORMAT = FILENAME_PREFIX + "_%s_%s" + FILENAME_TYPE;


    // member fields for saving objects:
    private String mFilename;
    private FileOutputStream mOutputStream;

    // internal data saving:
    private boolean mIsOpen;



    /*
     * Methods:
     */

    public abstract void log(String logEntry) throws  IOException;
    protected abstract  void writeHeadLine() throws IOException;





    public FPALogger(String filename_suffix) {
        mFilename = String.format(Locale.GERMAN,FILENAME_FORMAT, DATE_FORMAT_FILE_NAME.format(Calendar.getInstance().getTime()),filename_suffix);

        mIsOpen = false;
        mOutputStream = null;
    }


    public boolean open() throws IOException {

        // test:
        if(!isExternalStorageWritable()) { return false; }

        // open file
        File dir = getLogsStorageDir();
        if(dir == null) {return false;}
        File file = new File(dir, mFilename);
        if(file == null) {return false;}

        // open Stream:
        mOutputStream = new FileOutputStream(file);
        if(mOutputStream == null) {return false;}

        mIsOpen = true;

        // write header lines:
        writeHeadLine();
        writeToLogFile(HEADLINE_SEPARATOR);


        return mIsOpen;
    }

    protected void writeToLogFile(String string) throws  IOException {
        if(isOpen()) {
            try {
                mOutputStream.write(string.getBytes());
            } catch (Exception e){
                throw e;
            }
        }
    }



    public boolean isOpen() {
        return this.mIsOpen;
    }

    public boolean close() throws IOException{

        if(mOutputStream != null) {
            try {
                mOutputStream.close();
            } catch (IOException e) {
                throw e;
            }
        }
        return true;
    }


    /* Checks if external storage is available for read and write */
    private boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }


    public static File getLogsStorageDir() {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStorageDirectory(), LOG_FILE_DIR);
        file.mkdirs();
        return file;
    }



}
