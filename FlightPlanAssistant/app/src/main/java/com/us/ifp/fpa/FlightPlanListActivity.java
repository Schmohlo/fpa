package com.us.ifp.fpa;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.us.ifp.fpa.CrashReport.TopExceptionHandler;
import com.us.ifp.fpa.flightplanstuff.FlightPlan;
import com.us.ifp.fpa.flightplanstuff.FlightPlanDBHelper;
import com.us.ifp.fpa.flightplanstuff.FlightPlanDBWorker;
import com.us.ifp.fpa.flightplanstuff.FlightPlanException;
import com.us.ifp.fpa.flightplanstuff.FlightPlanParseException;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import dji.common.error.DJIError;
import dji.common.util.DJICommonCallbacks;

public class FlightPlanListActivity extends AppCompatActivity {

    private static final int READ_REQUEST_CODE = 42;
    private final String TAG = FlightPlanListActivity.class.getSimpleName();

    private FlightPlanDBWorker mDBWorker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Thread.setDefaultUncaughtExceptionHandler(new TopExceptionHandler(this));

        setContentView(R.layout.activity_flightplanlist);

        mDBWorker = ((FPAApplication) getApplication()).globalFlightPlanDBWorker;
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_flightplanlist, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            startActivity(new Intent(this,SettingsListActivity.class));
            return true;
        }

        if (id == R.id.action_about) {
            startActivity(new Intent(this,AboutActivity.class));
            return true;
        }

        if (id == R.id.action_addFlightPlan) {
            performFileSearch();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    /**
     * Fires an intent to spin up the "file chooser" UI and select an image.
     */
    public void performFileSearch() {

        // ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file
        // browser.
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);

        // Filter to only show results that can be "opened", such as a
        // file (as opposed to a list of contacts or timezones)
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        // Filter to show only images, using the image MIME data type.
        // If one wanted to search for ogg vorbis files, the type would be "audio/ogg".
        // To search for all documents available via installed storage providers,
        // it would be "*/*".
        intent.setType("text/plain");

        startActivityForResult(intent, READ_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {

        // The ACTION_OPEN_DOCUMENT intent was sent with the request code
        // READ_REQUEST_CODE. If the request code seen here doesn't match, it's the
        // response to some other intent, and the code below shouldn't run at all.

        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            // The document selected by the user won't be returned in the intent.
            // Instead, a URI to that document will be contained in the return intent
            // provided to this method as a parameter.
            // Pull that URI using resultData.getData().
            Uri uri = null;
            if (resultData != null) {
                uri = resultData.getData();
                Log.i(TAG, "Uri: " + uri.toString());

                final FlightPlan flightPlan = new FlightPlan();
                FlightPlan.ReadReport report = null;
                InputStream inputStream = null;

                try {
                    inputStream = getContentResolver().openInputStream(uri);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                try {
                    report = flightPlan.readFromFile(inputStream);
                } catch(FlightPlanParseException e) {
                    Toast.makeText(this,"Error while parsing input file:",Toast.LENGTH_SHORT).show();
                    Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();
                    return;
                }

                if (report != null) {

                    final FlightPlan.ReadReport freport = report;

                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    // Add the buttons
                    if (!report.errors) {
                        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // write to DB
                                mDBWorker.writeFlightPlanToDB(flightPlan);
                                try {
                                    mDBWorker.logger.log(freport);
                                } catch (IOException e) {
                                    Log.e(TAG,Log.getStackTraceString(e));
                                }
                                Fragment fragment = getFragmentManager().findFragmentById(R.id.fragment_FlightPlanList);
                                if( fragment != null ) {
                                    ((FlightPlanListFragment) fragment).renewList();
                                }
                            }
                        });
                    }
                    builder.setNegativeButton(R.string.cancel, null);
                    // Chain together various setter methods to set the dialog characteristics
                    builder.setMessage(report.checkMessage)
                            .setTitle("Confirm flight plan before saving");

                    // Create the AlertDialog
                    AlertDialog dialog = builder.create();
                    dialog.show();

                }

            }
        }
    }



}
