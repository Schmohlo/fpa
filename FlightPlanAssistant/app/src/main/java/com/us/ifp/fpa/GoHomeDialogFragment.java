package com.us.ifp.fpa;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Locale;

import static android.view.View.GONE;


public class GoHomeDialogFragment extends DialogFragment {



    /* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    public interface NoticeDialogListenerGoHome {
        public void onGoHomeDialogPositiveClick(DialogFragment dialog);
        public void onGoHomeDialogNegativeClick(DialogFragment dialog);
    }

    // Use this instance of the interface to deliver action events
    NoticeDialogListenerGoHome mListener;

    /* Used to communicate, that the inputs are correct to the activity/fragment that called
    this dialog, so that there the Ok-Button can be set accordingly. */
    private boolean input_GoHomeAltitude_ok;

    // constant values for preferences etc:
    public static final String PREFS_NAME            = "GoHomeDialogPrefsFile";
    public static final String PREF_GoHomeAltitude   = "GoHomeAltitude";


    // inner cass communication:
    private Button button_OK;



    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        // builder for convenient dialog building:
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());


        // Get the layout inflater:
        LayoutInflater inflater = getActivity().getLayoutInflater();


        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View view = inflater.inflate(R.layout.dialog_fragment_go_home, null);


        /* save EditTexts to static variables, so they can be accessed in the callback functions
        of the buttons and change listeners */
        final EditText editText_goHomeAltitude  = ((EditText) view.findViewById(R.id.editText_GoHomeAltitude));


        //
        String positiveButtonText = getString(R.string.goHome);
        if(getActivity() instanceof  SettingsListActivity) {
            positiveButtonText = getString(R.string.save);
        }

        // build the dialog (this has to be up here, so the OK-Button can be accessed in the
        // input change listener to enable/disable:
        builder.setView(view)
                // Add action buttons
                .setPositiveButton(positiveButtonText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        // save preferences for next use:
                        SharedPreferences settings = getActivity().getSharedPreferences(GoHomeDialogFragment.PREFS_NAME, 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString(PREF_GoHomeAltitude, editText_goHomeAltitude.getText().toString());
                        editor.commit();

                        mListener.onGoHomeDialogPositiveClick(GoHomeDialogFragment.this);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        mListener.onGoHomeDialogNegativeClick(GoHomeDialogFragment.this);
                    }
                });


        // create dialog:
        AlertDialog dialog = builder.create();
        // dialog.show() so that the Buttons get initialized. This doesn't display the dialog
        // however.
        dialog.show();


        /* save TextViews, the OK-Button and the number of waypoints in this flight plan to static
           variables, so they can be accessed in the callback functions of the buttons and change
           listeners */
        final TextView textView_GoHomeAltitude_Message  = ((TextView) view.findViewById(R.id.textView_GoHomeAltitude_Message));
        button_OK                                       = dialog.getButton(Dialog.BUTTON_POSITIVE);


        /* Mechanism for checking inputs. Important: this block of code has to be before setting
           default values, so that they can be checked and displayed as wrong, also. */
        ((EditText) view.findViewById(R.id.editText_GoHomeAltitude)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                textView_GoHomeAltitude_Message.setText("");
                textView_GoHomeAltitude_Message.setVisibility(GONE);
                set_input_GoHomeAltitude_ok(true);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = editable.toString();
                try {
                    float content = Float.parseFloat(text);
                    if (content < 20 || content > 500) {
                        set_input_GoHomeAltitude_ok(false);
                        textView_GoHomeAltitude_Message.setVisibility(View.VISIBLE);
                        textView_GoHomeAltitude_Message.setText(String.format(Locale.GERMAN, "Must be between 20 and 500"));
                    }
                } catch (NumberFormatException e) {
                    set_input_GoHomeAltitude_ok(false);
                    textView_GoHomeAltitude_Message.setVisibility(View.VISIBLE);
                    textView_GoHomeAltitude_Message.setText(String.format(Locale.GERMAN, "Not a valid float!"));
                }
            }
        });

        // get settings for previously used input data:
        SharedPreferences settings = getActivity().getSharedPreferences(this.PREFS_NAME, 0);


        // set default values:
        ((EditText) view.findViewById(R.id.editText_GoHomeAltitude)).setText(settings.getString(PREF_GoHomeAltitude, "50"));



        return dialog;
    }



    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListenerGoHome
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListenerGoHome so we can send events to the host
            mListener = (NoticeDialogListenerGoHome) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListenerGoHome");
        }
    }







    private void set_input_GoHomeAltitude_ok(boolean value) {
        input_GoHomeAltitude_ok = value;
        setOKButtonEnabled();
    }


    private void setOKButtonEnabled() {
        button_OK.setEnabled(inputOK());
    }

    private boolean inputOK() {
        return input_GoHomeAltitude_ok;
    }
}

