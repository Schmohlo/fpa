package com.us.ifp.fpa.CrashReport;

import android.app.Activity;
import android.content.Context;
import android.os.Environment;

import com.us.ifp.fpa.MissionControl.FPALogger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by V582 on 29.03.2017.
 */


//


public class TopExceptionHandler implements Thread.UncaughtExceptionHandler {

    // Use
    // 'Thread.setDefaultUncaughtExceptionHandler(new TopExceptionHandler(this));'
    // in onCreate(...) of every activity.


    private Thread.UncaughtExceptionHandler defaultUEH;
    private Activity app = null;



    public TopExceptionHandler(Activity app) {
        this.defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
        this.app = app;
    }

    public void uncaughtException(Thread t, Throwable e) {
        StackTraceElement[] arr = e.getStackTrace();
        String report = e.toString()+"\n\n";
        report += "--------- Stack trace ---------\n\n";
        for (int i=0; i<arr.length; i++) {
            report += "    "+arr[i].toString()+"\n";
        }
        report += "-------------------------------\n\n";

        // If the exception was thrown in a background thread inside
        // AsyncTask, then the actual exception can be found with getCause

        report += "--------- Cause ---------\n\n";
        Throwable cause = e.getCause();
        if(cause != null) {
            report += cause.toString() + "\n\n";
            arr = cause.getStackTrace();
            for (int i=0; i<arr.length; i++) {
                report += "    "+arr[i].toString()+"\n";
            }
        }
        report += "-------------------------------\n\n";

        try {

            CrashLogger crashLogger = new CrashLogger();
            crashLogger.open();
            crashLogger.log(report);
            crashLogger.close();

        } catch(IOException ioe) {
            // ...
        }

        defaultUEH.uncaughtException(t, e);
    }
}