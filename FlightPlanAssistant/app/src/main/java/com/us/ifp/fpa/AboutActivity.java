package com.us.ifp.fpa;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.us.ifp.fpa.CrashReport.TopExceptionHandler;
import com.us.ifp.fpa.MissionControl.MCLogger;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import dji.common.error.DJIError;
import dji.common.util.DJICommonCallbacks;
import dji.sdk.flightcontroller.DJIFlightController;
import dji.sdk.products.DJIAircraft;

public class AboutActivity extends AppCompatActivity {


    private final String LOG_TAG = AboutActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Thread.setDefaultUncaughtExceptionHandler(new TopExceptionHandler(this));

        setContentView(R.layout.activity_about);


        Date buildDate = new Date(BuildConfig.TIMESTAMP);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        String timeString  = simpleDateFormat.format(buildDate.getTime());


        TextView textViewVersion   = (TextView) findViewById(R.id.textView_Version);
        TextView textViewBuildDate = (TextView) findViewById(R.id.textView_buildDate);

        textViewVersion  .setText(textViewVersion  .getText() + BuildConfig.VERSION_NAME);
        textViewBuildDate.setText(textViewBuildDate.getText() + timeString);
    }


    // Using this helper method to show Toasts, invoked by other threads.
    public void showToast(final String msg, int length) {
        if(length != Toast.LENGTH_LONG || length != Toast.LENGTH_SHORT) {
            length = Toast.LENGTH_SHORT;
        }
        final int lLength = length; // other thread => needs to be final;
        runOnUiThread(new Runnable() {
            @SuppressWarnings("WrongConstant")
            public void run() {
                Toast.makeText(getBaseContext(), msg, lLength).show();
                Log.i(LOG_TAG,msg);
            }
        });
    }


}
