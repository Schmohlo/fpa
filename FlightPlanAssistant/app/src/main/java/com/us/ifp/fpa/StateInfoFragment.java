package com.us.ifp.fpa;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.us.ifp.fpa.GeodeticToolkit.GeodeticToolkit;
import com.us.ifp.fpa.MissionControl.MCLogger;
import com.us.ifp.fpa.MissionControl.MissionControl;
import com.us.ifp.fpa.MissionControl.MissionControlInterface;

import java.util.Locale;

import dji.common.flightcontroller.DJIAircraftRemainingBatteryState;
import dji.common.flightcontroller.DJIAttitude;
import dji.common.flightcontroller.DJIFlightControllerCurrentState;
import dji.common.flightcontroller.DJIGPSSignalStatus;
import dji.common.flightcontroller.DJILocationCoordinate2D;
import dji.common.flightcontroller.DJILocationCoordinate3D;
import dji.common.flightcontroller.DJISimulatorStateData;
import dji.common.gimbal.DJIGimbalState;
import dji.sdk.flightcontroller.DJIFlightControllerDelegate;
import dji.sdk.flightcontroller.DJISimulator;
import dji.sdk.gimbal.DJIGimbal;
import dji.sdk.missionmanager.DJIMission;
import dji.sdk.missionmanager.DJIWaypointMission;


public class StateInfoFragment extends Fragment {


    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String LOG_TAG = StateInfoFragment.class.getName();


    public MissionControlInterface mMissionControl;


    public StateInfoFragment() {
        // Required empty public constructor
    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();

        Log.i("infoFragmentDestroyed",LOG_TAG);

        // unregister the callbacks! (or else the app would crash everytime the fragment is destroyed)
        mMissionControl.getAircraft().getGimbal().setGimbalStateUpdateCallback(null);
        mMissionControl.getSimulator().setUpdatedSimulatorStateDataCallback(null);
        mMissionControl.getFlightControler().setUpdateSystemStateCallback(null);

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment:
        View rootView = inflater.inflate(R.layout.fragment_state_info, container, false);


        /** Anonymous callback classes for Status Updates:
         *
         * Because these Callbacks will be run in another thread, I have to get the UI-Thread for
         * updating the UI (getting infos from the UI doesn't need this) by using a handler.
         *
         * Variables used in the Handler.post callback for UI updating that depend on the
         * state-paramters, must be declared final outside of .post because they will be grabed by
         * an external method / thread.
         */

        // Callback for Simulator State Updates:
        mMissionControl.getSimulator().setUpdatedSimulatorStateDataCallback(new DJISimulator.UpdatedSimulatorStateDataCallback() {
            @Override
            public void onSimulatorDataUpdated(DJISimulatorStateData djiSimulatorStateData) {

               // mMissionControl.log("In Simulation.", MCLogger.LogType.i);

                final DJISimulatorStateData simstate = djiSimulatorStateData;

                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        // it can happen, that this method is still running while the fragment
                        // (the activity has been closed).
                        if(getActivity() == null) return;

                        TextView lv1 = (TextView) getActivity().findViewById(R.id.TextView_SimMotor_Data);
                        lv1.setText(String.format(Locale.GERMAN, "%s", (simstate.areMotorsOn()? "on":"off")));

                        lv1 = (TextView) getActivity().findViewById(R.id.TextView_SimLatLong_Data);
                        lv1.setText(String.format(Locale.GERMAN, "%1$.7f° / %2$.7f°", simstate.getLatitude(), simstate.getLongitude()));

                        lv1 = (TextView) getActivity().findViewById(R.id.TextView_SimXYZ_Data);
                        lv1.setText(String.format(Locale.GERMAN, "%1$.2fm / %2$.2fm / %3$.2fm", simstate.getPositionX(),simstate.getPositionY(), simstate.getPositionZ()));

                        lv1 = (TextView) getActivity().findViewById(R.id.TextView_SimHRP_Data);
                        lv1.setText(String.format(Locale.GERMAN, "%1$.0f° / %2$.0f° / %3$.0f°", simstate.getYaw(),simstate.getRoll(), simstate.getPitch()));

                        lv1 = (TextView) getActivity().findViewById(R.id.TextView_SimFlying_Data);
                        lv1.setText(String.format(Locale.GERMAN, "%s", simstate.isFlying()? "flying": "not flying"));
                    }
                });
            }
        });


        // Callback for Gimbal State Updates
        mMissionControl.getAircraft().getGimbal().setGimbalStateUpdateCallback(new DJIGimbal.GimbalStateUpdateCallback() {
            @Override
            public void onGimbalStateUpdate(DJIGimbal djiGimbal, DJIGimbalState djiGimbalState) {

                final float lPitch = djiGimbalState.getAttitudeInDegrees().pitch;

                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {

                        // it can happen, that this method is still running while the fragment
                        // (the activity has been closed).
                        if(getActivity() == null) return;

                        TextView lv1 = (TextView) getActivity().findViewById(R.id.TextView_GimbalPitch_Data);
                        lv1.setText(String.format(Locale.GERMAN, "%.0f°", lPitch));
                    }
                });
            }
        });


        // Callback for FlightController / Aircraft State Updates
        mMissionControl.getFlightControler().setUpdateSystemStateCallback(new DJIFlightControllerDelegate.FlightControllerUpdateSystemStateCallback () {

            @Override
            public void onResult(DJIFlightControllerCurrentState nstate) {

                final double satCount = nstate.getSatelliteCount();
                final DJILocationCoordinate3D koords = nstate.getAircraftLocation();
                final DJILocationCoordinate2D home  = nstate.getHomeLocation();
                final DJIAttitude attitude = nstate.getAttitude();
                final DJIGPSSignalStatus gpsSS = nstate.getGpsSignalStatus();
                final DJIAircraftRemainingBatteryState batteryState = nstate.getRemainingBattery();
                final boolean isflying = nstate.isFlying();


                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        // Here comes the interesting part of updating:

                        // it can happen, that this method is still running while the fragment
                        // (the activity has been closed).
                        if(getActivity() == null) return;

                        TextView lv1 = (TextView) getActivity().findViewById(R.id.TextView_CurrentFlightPlan_Data);
                        lv1.setText(String.format(Locale.GERMAN, "%s (ID:%d)", ((MainActivity) getActivity()).getCurrentFlightPlanName(), ((MainActivity) getActivity()).getCurrentFlightPlanID()));

                        lv1 = (TextView) getActivity().findViewById(R.id.TextView_Notification_Data);
                        lv1.setText(String.format(Locale.GERMAN, "%s", ((MainActivity) getActivity()).getNotification()));

                        lv1 = (TextView) getActivity().findViewById(R.id.TextView_BatteryState_Data);
                        lv1.setText(String.format(Locale.GERMAN, "%s (%d%%)", batteryState.toString(), mMissionControl.getProductEnergyRemainingPercent()));

                        lv1 = (TextView) getActivity().findViewById(R.id.TextView_BatteryStateRC_Data);
                        lv1.setText(String.format(Locale.GERMAN, "%d%%", mMissionControl.getRemoteControllerEnergyRemainingPercent()));


                        updateMissionStateInfo();


                        lv1 = (TextView) getActivity().findViewById(R.id.TextView_SimState_Data);
                        lv1.setText( mMissionControl.hasSimulatorStarted()? "on":"off");

                        lv1 = (TextView) getActivity().findViewById(R.id.TextView_GPSStatus_Data);
                        lv1.setText(gpsSS.toString());

                        ImageView iv1 = (ImageView) getActivity().findViewById(R.id.imageView_GPSStatus);
                        if(gpsSS.equals(DJIGPSSignalStatus.None)) {
                            iv1.setImageResource(R.drawable.gpssignal_none_or);
                        } else if(gpsSS.equals(DJIGPSSignalStatus.Level0)) {
                            iv1.setImageResource(R.drawable.gpssignal_level0_or);
                        } else if(gpsSS.equals(DJIGPSSignalStatus.Level1)) {
                            iv1.setImageResource(R.drawable.gpssignal_level1_or);
                        } else if(gpsSS.equals(DJIGPSSignalStatus.Level2)) {
                            iv1.setImageResource(R.drawable.gpssignal_level2_or);
                        } else if(gpsSS.equals(DJIGPSSignalStatus.Level3)) {
                            iv1.setImageResource(R.drawable.gpssignal_level3_or);
                        } else if(gpsSS.equals(DJIGPSSignalStatus.Level4)) {
                            iv1.setImageResource(R.drawable.gpssignal_level4_or);
                        } else if(gpsSS.equals(DJIGPSSignalStatus.Level5)) {
                            iv1.setImageResource(R.drawable.gpssignal_level5_or);
                        }

                        lv1 = (TextView) getActivity().findViewById(R.id.TextView_SatCount_Data);
                        lv1.setText(String.format(Locale.GERMAN, "%.0f", satCount));

                        lv1 = (TextView) getActivity().findViewById(R.id.TextView_Altitude_Data);
                        lv1.setText(String.format(Locale.GERMAN, "%.2f m", koords.getAltitude()));

                        lv1 = (TextView) getActivity().findViewById(R.id.TextView_LatLong_Data);
                        lv1.setText(String.format(Locale.GERMAN, "%1$.7f° / %2$.7f°", koords.getLatitude(), koords.getLongitude()));

                        lv1 = (TextView) getActivity().findViewById(R.id.TextView_HPR_Data);
                        lv1.setText(String.format(Locale.GERMAN, "%1$.0f° / %2$.0f° / %3$.0f°",attitude.yaw,attitude.pitch,attitude.roll ));


                        // Im using here a very simple approach to convert Lat/Long to
                        // X/Y.
                        double[] dXY = GeodeticToolkit.dLL2dXY(
                                koords.getLatitude() -home.getLatitude(),
                                koords.getLongitude()-home.getLongitude(),home.getLatitude());

                        lv1 = (TextView) getActivity().findViewById(R.id.TextView_XY_Data);
                        if (!Double.isNaN(home.getLatitude())) {
                            lv1.setText(String.format(Locale.GERMAN, "%1$.2fm / %2$.2fm", dXY[0], dXY[1]));
                        } else {
                            lv1.setText(String.format(Locale.GERMAN, "No Homepoint was set"));
                        }


                        /* Workaround for there not being a callback when the Aircraft has landed etc*/
                        TextView button_takeOff = (TextView) getActivity().findViewById(R.id.button_TakeOff);
                        TextView button_land    = (TextView) getActivity().findViewById(R.id.button_Land);
                        TextView button_goHome  = (TextView) getActivity().findViewById(R.id.button_GoHome);
                        TextView button_setHome = (TextView) getActivity().findViewById(R.id.button_SetHome);


                        if(button_takeOff != null) {
                            if (mMissionControl.isFlying() && !mMissionControl.isTakingOff()) {
                                ((Button) button_takeOff).setEnabled(false);
                            } else {
                                ((Button) button_takeOff).setEnabled(true);
                            }

                            if (mMissionControl.isTakingOff()) {
                                button_takeOff.setText(R.string.takeOff_Cancel);
                            } else {
                                button_takeOff.setText(R.string.takeOff);
                            }
                        }
                        if (button_land != null) {
                            if (mMissionControl.isFlying()) {
                                ((Button) button_land).setEnabled(true);
                            } else {
                                ((Button) button_land).setEnabled(false);
                            }

                            if (mMissionControl.isGoingHome()) {
                                button_land.setText(R.string.land);
                            }
                            if (mMissionControl.isLanding()) {
                                button_land.setText(R.string.land_Cancel);
                            }
                            if (!mMissionControl.isGoingHome() && !mMissionControl.isLanding()) {
                                button_land.setText(R.string.land);
                            }
                        }
                        if (button_goHome != null) {
                            if (mMissionControl.isFlying()) {
                                ((Button) button_goHome).setEnabled(true);
                            } else {
                                ((Button) button_goHome).setEnabled(false);
                            }

                            if (mMissionControl.isGoingHome()) {
                                button_goHome.setText(R.string.goHome_Cancel);
                            }
                            if (mMissionControl.isLanding()) {
                                button_goHome.setText(R.string.goHome);
                            }
                            if (!mMissionControl.isGoingHome() && !mMissionControl.isLanding()) {
                                button_goHome.setText(R.string.goHome);
                            }
                        }
                        if (button_setHome != null) {
                            if (mMissionControl.isFlying()) {
                                ((Button) button_setHome).setEnabled(true);
                            } else {
                                ((Button) button_setHome).setEnabled(false);
                            }
                        }





                        /** Workaround for the StartSimulator-Callback that isn't called*/
                        lv1 = (TextView) getActivity().findViewById(R.id.button_SSSim);
                        if(mMissionControl.hasSimulatorStarted()) {
                            getActivity().findViewById(R.id.RelativeLayout_SimStateInfoTable).setVisibility(View.VISIBLE);
                            if(lv1 != null) {
                                lv1.setText(R.string.simStop);
                                if (lv1.getText().equals(getString(R.string.simStart))) {
                                    Toast.makeText(getActivity(), "Simulation started.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        } else {
                            getActivity().findViewById(R.id.RelativeLayout_SimStateInfoTable).setVisibility(View.GONE);
                            if(lv1 != null) {
                                lv1.setText(R.string.simStart);
                                if (lv1.getText().equals(getString(R.string.simStop))) {
                                    Toast.makeText(getActivity(), "Simulation stopped.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }


                    }
                });

            }
            });
        // start status updates:
        // not needed, implied by setting callback (doc is incorrect) (answer found in dji
        // forum).


        return rootView;
    }


    // extra method so it can be evoked by MissionPrepareStatus etc.
    public void updateMissionStateInfo() {

        TextView lv1 = (TextView) getActivity().findViewById(R.id.TextView_MissionState_Data);
        lv1.setText(mMissionControl.getMissionStateInfoText());

    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
         // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListenerStartSim so we can send events to the host
            mMissionControl = ((MainActivity) activity).missionControl;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement MissionControlStateInterface");
        }
    }



}
