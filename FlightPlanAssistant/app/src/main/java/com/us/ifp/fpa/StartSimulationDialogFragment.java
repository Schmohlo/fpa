package com.us.ifp.fpa;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Locale;

import static android.view.View.GONE;


public class StartSimulationDialogFragment extends DialogFragment {



    /* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    public interface NoticeDialogListenerStartSim {
        public void onStartSimDialogPositiveClick(DialogFragment dialog);
        public void onStartSimDialogNegativeClick(DialogFragment dialog);
    }

    // Use this instance of the interface to deliver action events
    NoticeDialogListenerStartSim mListener;

    /* Used to communicate, that the inputs are correct to the activity/fragment that called
    this dialog, so that there the Ok-Button can be set accordingly. */
    private boolean input_HomePointLat_ok;
    private boolean input_HomePointLon_ok;
    private boolean input_SimSatCount_ok;

    // constant values for preferences etc:
    public static final String PREFS_NAME          = "StartSimulationDialogPrefsFile";
    public static final String PREF_HomePointLat   = "HomePointLat";
    public static final String PREF_HomePointLon   = "HomePointLon";
    public static final String PREF_SimSatCount    = "SimSatCount";

    // inner cass communication:
    private Button button_OK;



    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        // builder for convenient dialog building:
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());


        // Get the layout inflater:
        LayoutInflater inflater = getActivity().getLayoutInflater();


        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View view = inflater.inflate(R.layout.dialog_fragment_start_simulation, null);


        /* save EditTexts to static variables, so they can be accessed in the callback functions
        of the buttons and change listeners */
        final EditText editText_homePointLatitude  = ((EditText) view.findViewById(R.id.editText_homePointLatitude));
        final EditText editText_homePointLongitude = ((EditText) view.findViewById(R.id.editText_homePointLongitude));
        final EditText editText_SimSatCount        = ((EditText) view.findViewById(R.id.editText_SimSatCount));


        // build the dialog (this has to be up here, so the OK-Button can be accessed in the
        // input change listener to enable/disable:
        builder.setView(view)
                // Add action buttons
                .setPositiveButton(R.string.StartSimulation, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        // save preferences for next use:
                        SharedPreferences settings = getActivity().getSharedPreferences(StartSimulationDialogFragment.PREFS_NAME, 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString(PREF_HomePointLat, editText_homePointLatitude.getText().toString());
                        editor.putString(PREF_HomePointLon, editText_homePointLongitude.getText().toString());
                        editor.putString(PREF_SimSatCount , editText_SimSatCount.getText().toString());
                        editor.commit();

                        mListener.onStartSimDialogPositiveClick(StartSimulationDialogFragment.this);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        mListener.onStartSimDialogNegativeClick(StartSimulationDialogFragment.this);
                    }
                });


        // create dialog:
        AlertDialog dialog = builder.create();
        // dialog.show() so that the Buttons get initialized. This doesn't display the dialog
        // however.
        dialog.show();


        /* save TextViews, the OK-Button and the number of waypoints in this flight plan to static
           variables, so they can be accessed in the callback functions of the buttons and change
           listeners */
        final TextView textView_homePointLatitude_Message  = ((TextView) view.findViewById(R.id.textView_homePointLatitude_Message));
        final TextView textView_homePointLongitude_Message = ((TextView) view.findViewById(R.id.textView_homePointLongitude_Message));
        final TextView textView_SimSatCount_Message        = ((TextView) view.findViewById(R.id.textView_SimSatCount_Message));
        button_OK                                          = dialog.getButton(Dialog.BUTTON_POSITIVE);


        /* Mechanism for checking inputs. Important: this block of code has to be before setting
           default values, so that they can be checked and displayed as wrong, also. */
        ((EditText) view.findViewById(R.id.editText_homePointLatitude)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                textView_homePointLatitude_Message.setText("");
                textView_homePointLatitude_Message.setVisibility(GONE);
                set_input_HomePointLat_ok(true);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = editable.toString();
                try {
                    double content = Double.parseDouble(text);
                    if (content < -90 || content > 90) {
                        set_input_HomePointLat_ok(false);
                        textView_homePointLatitude_Message.setVisibility(View.VISIBLE);
                        textView_homePointLatitude_Message.setText(String.format(Locale.GERMAN, "Must be between -90 and 90"));
                    }
                } catch (NumberFormatException e) {
                    set_input_HomePointLat_ok(false);
                    textView_homePointLatitude_Message.setVisibility(View.VISIBLE);
                    textView_homePointLatitude_Message.setText(String.format(Locale.GERMAN, "Not a valid double "));
                }
            }
        });
        ((EditText) view.findViewById(R.id.editText_homePointLongitude)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                textView_homePointLongitude_Message.setText("");
                textView_homePointLongitude_Message.setVisibility(GONE);
                set_input_HomePointLon_ok(true);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = editable.toString();
                try {
                    double content = Double.parseDouble(text);
                    if (content < -360 || content > 360) {
                        set_input_HomePointLon_ok(false);
                        textView_homePointLongitude_Message.setVisibility(View.VISIBLE);
                        textView_homePointLongitude_Message.setText(String.format(Locale.GERMAN, "Must be between -360 and 360"));
                    }
                } catch (NumberFormatException e) {
                    set_input_HomePointLon_ok(false);
                    textView_homePointLongitude_Message.setVisibility(View.VISIBLE);
                    textView_homePointLongitude_Message.setText(String.format(Locale.GERMAN, "Not a valid double "));
                }
            }
        });
        ((EditText) view.findViewById(R.id.editText_SimSatCount)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                textView_SimSatCount_Message.setText("");
                textView_SimSatCount_Message.setVisibility(GONE);
                set_input_SimSatCount_ok(true);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = editable.toString();
                try {
                    int content = Integer.parseInt(text);
                    if (content < 0 || content > 20) {
                        set_input_SimSatCount_ok(false);
                        textView_SimSatCount_Message.setVisibility(View.VISIBLE);
                        textView_SimSatCount_Message.setText(String.format(Locale.GERMAN, "Must be between 0 and 20"));
                    }
                } catch (NumberFormatException e) {
                    set_input_SimSatCount_ok(false);
                    textView_SimSatCount_Message.setVisibility(View.VISIBLE);
                    textView_SimSatCount_Message.setText(String.format(Locale.GERMAN, "Not a valid double "));
                }
            }
        });


        // get settings for previously used input data:
        SharedPreferences settings = getActivity().getSharedPreferences(this.PREFS_NAME, 0);


        // set default values:
        ((EditText) view.findViewById(R.id.editText_homePointLatitude)).setText(settings.getString(PREF_HomePointLat, "0"));
        ((EditText) view.findViewById(R.id.editText_homePointLongitude)).setText(settings.getString(PREF_HomePointLon, "0"));
        ((EditText) view.findViewById(R.id.editText_SimSatCount)).setText(settings.getString(PREF_SimSatCount, "0"));


        return dialog;
    }



    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListenerStartSim
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListenerStartSim so we can send events to the host
            mListener = (NoticeDialogListenerStartSim) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListenerStartSim");
        }
    }







    private void set_input_HomePointLat_ok(boolean value) {
        input_HomePointLat_ok = value;
        setOKButtonEnabled();
    }

    private void set_input_HomePointLon_ok(boolean value) {
        input_HomePointLon_ok = value;
        setOKButtonEnabled();
    }

    private void set_input_SimSatCount_ok(boolean value) {
        input_SimSatCount_ok = value;
        setOKButtonEnabled();
    }

    private void setOKButtonEnabled() {
        button_OK.setEnabled(inputOK());
    }

    private boolean inputOK() {
        return input_HomePointLat_ok && input_HomePointLon_ok && input_SimSatCount_ok;
    }
}

