package com.us.ifp.fpa;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.us.ifp.fpa.CrashReport.TopExceptionHandler;
import com.us.ifp.fpa.MissionControl.MCLogger;
import com.us.ifp.fpa.MissionControl.MissionControl;

import org.w3c.dom.Text;

import dji.common.error.DJIError;
import dji.common.util.DJICommonCallbacks;
import dji.sdk.base.DJIBaseProduct;
import dji.sdk.products.DJIAircraft;
import dji.sdk.sdkmanager.DJISDKManager;

/**
 * Created by V582 on 22.11.2016.
 */

public class ConnectionActivity extends AppCompatActivity
                                implements View.OnClickListener{

    private static final String LOG_TAG = ConnectionActivity.class.getName();

    private TextView mTextConnectionStatus;
    private TextView mTextProduct;
    private TextView mTextIsAircraft;
    private TextView mTextDJISDKVersion;
    private Button   mBtnOpen;


    private static final int GET_FP_REQUEST_CODE = 12345;






    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {



        if (requestCode == GET_FP_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if(resultData != null) {
                long id = resultData.getLongExtra(FlightPlanListFragment.EXTRA_RETURN_ID,-1);
                Toast.makeText(this,"ID: " + Long.toString(id),Toast.LENGTH_SHORT ).show();


                // create Dialog for inputs to create a mission:
                DialogFragment dialogFragment = new CreateMissionDialogFragment();
                Bundle args = new Bundle();
                args.putLong(CreateMissionDialogFragment.ARGUMENT_ID, id);
                dialogFragment.setArguments(args);
                dialogFragment.show(getFragmentManager(), "");

            }

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_connectionactivity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == R.id.action_offsettool) {
            startActivity(new Intent(this,OffsetToolActivity.class));
            return true;
        }

        if (id == R.id.action_settings) {
            startActivity(new Intent(this,SettingsListActivity.class));
            return true;
        }

        if (id == R.id.action_about) {
            startActivity(new Intent(this,AboutActivity.class));
            return true;
        }

        if (id == R.id.action_manageFlightPlans) {
            startActivity(new Intent(this,FlightPlanListActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);

        Thread.setDefaultUncaughtExceptionHandler(new TopExceptionHandler(this));

        // When the compile and target version is higher than 22, please request the following
        // permissions at runtime to ensure the SDK works well.
        // Here commented, because I'm working right now with ADK 22 due to compability problems
/*        if (Build.VERSION.SDK_INT > 22) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.VIBRATE,
                            Manifest.permission.INTERNET, Manifest.permission.ACCESS_WIFI_STATE,
                            Manifest.permission.WAKE_LOCK, Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.CHANGE_WIFI_STATE, Manifest.permission.MOUNT_UNMOUNT_FILESYSTEMS,
                            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.SYSTEM_ALERT_WINDOW,
                            Manifest.permission.READ_PHONE_STATE,
                    }
                    , 1);
        }*/

        setContentView(R.layout.activity_connection);
        initUI();

        // Register the broadcast receiver for receiving the device connection's changes.
        // The Broadcast is send by the FPAApplication-class.
        IntentFilter filter = new IntentFilter();
        filter.addAction(FPAApplication.FLAG_CONNECTION_CHANGE);
        registerReceiver(mReceiver, filter);

        ((FPAApplication) getApplication()).setmCurrentActivity(this);

    }




    @Override
    protected void onDestroy() {
        Log.v(LOG_TAG, "onDestroy");
        unregisterReceiver(mReceiver);
        super.onDestroy();
    }



    private void initUI() {
        mTextConnectionStatus = (TextView) findViewById(R.id.text_connection_status);
        mTextProduct          = (TextView) findViewById(R.id.text_product_info);
        mTextIsAircraft       = (TextView) findViewById(R.id.textView_IsAircraft);
        mTextDJISDKVersion    = (TextView) findViewById(R.id.text_djisdk_version);
        mTextDJISDKVersion.setText(
                getString(R.string.sdk_version) + " " +
                        DJISDKManager.getInstance().getSDKVersion());

        mBtnOpen = (Button) findViewById(R.id.btn_open);
        mBtnOpen.setOnClickListener(this);
        mBtnOpen.setEnabled(false);
    }


    // mReceiver will receive Intents send as a Broadcast. It is used to catch device connection
    // changes.
    protected BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            refreshSDKRelativeUI();
        }
    };


    private void refreshSDKRelativeUI() {
        DJIBaseProduct product = FPAApplication.getProductInstance();
        if (null != product && product.isConnected()) {
            Log.v(LOG_TAG, "refreshSDK: True");

            String str = product instanceof DJIAircraft ? "DJIAircraft" : "DJIHandHeld";
            mTextConnectionStatus.setText("Status: " + str + "connected");
            if(str.equals("DJIAircraft")) {
                mBtnOpen.setEnabled(true);
            } else {
                mTextIsAircraft.setText("(only DJIAircraft are supported by this App)");
            }

            if (null != product.getModel()) {
                mTextProduct.setText("" + product.getModel().getDisplayName()); // what is
                // the """" for ?
            } else {
                mTextProduct.setText(R.string.product_information);
            }
        } else {
            Log.v(LOG_TAG, "refreshSDK: false");
            mBtnOpen.setEnabled(false);
            mTextProduct.setText(R.string.product_information);
            mTextConnectionStatus.setText(R.string.connection_loose);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_open: {

                // Create Mission Control and give to the Application. Here there should be a
                // aircraft connected. If otherwise, the MissionControl would probably crash.
                FPAApplication myapp = (FPAApplication) getApplication();
                myapp.setMissionControl(new MissionControl((DJIAircraft) myapp.getProductInstance()));

                myapp.getMissionControl().openLog();

                // set GoHomeAltitude to saved setting:
                SharedPreferences settings = getSharedPreferences(GoHomeDialogFragment.PREFS_NAME, 0);
                final float goHomeAltitude = Float.parseFloat(settings.getString(GoHomeDialogFragment.PREF_GoHomeAltitude,"50"));
                ((FPAApplication) getApplication()).getMissionControl().getFlightControler().setGoHomeAltitude(goHomeAltitude, new DJICommonCallbacks.DJICompletionCallback() {
                    @Override
                    public void onResult(DJIError djiError) {
                        if( djiError == null) {
                            showToast(String.format("GoHomeAltitude set to %.2f",goHomeAltitude),Toast.LENGTH_LONG, "SettingHomeAtAppStart", MCLogger.LogType.i);
                        } else {
                            showToast(djiError.getDescription(),Toast.LENGTH_LONG, "SettingHomeAtAppStart", MCLogger.LogType.e);
                        }
                    }
                });

                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;
            }
            default:
                break;
        }
    }


    // Using this helper method to show Toasts, invoked by other threads.
    public void showToast(final String msg, int length, final String caller, final MCLogger.LogType logType) {
        if(length != Toast.LENGTH_LONG && length != Toast.LENGTH_SHORT) {
            length = Toast.LENGTH_SHORT;
        }
        final int lLength = length; // other thread => needs to be final;
        runOnUiThread(new Runnable() {
            @SuppressWarnings("WrongConstant")
            public void run() {
                Toast.makeText(getBaseContext(), msg, lLength).show();
                ((FPAApplication) getApplication()).getMissionControl().log(caller + ": " + msg, logType);
                Log.i(LOG_TAG,msg);
            }
        });
    }

    // Using this helper method to show Toasts, invoked by other threads.
    public void showToast(final String msg, int length, final String caller, final MCLogger.LogType logType, final String additionalData) {
        if(length != Toast.LENGTH_LONG && length != Toast.LENGTH_SHORT) {
            length = Toast.LENGTH_SHORT;
        }
        final int lLength = length; // other thread => needs to be final;
        runOnUiThread(new Runnable() {
            @SuppressWarnings("WrongConstant")
            public void run() {
                Toast.makeText(getBaseContext(), msg, lLength).show();
                ((FPAApplication) getApplication()).getMissionControl().log(caller + ": " + msg + ". " + additionalData, logType);
                Log.i(LOG_TAG,msg);
            }
        });
    }
}
