package com.us.ifp.fpa;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.SurfaceTexture;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.TextureView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.us.ifp.fpa.CrashReport.TopExceptionHandler;
import com.us.ifp.fpa.GeodeticToolkit.GeodeticToolkit;
import com.us.ifp.fpa.MissionControl.MCLogger;
import com.us.ifp.fpa.MissionControl.MissionControl;
import com.us.ifp.fpa.flightplanstuff.FlightPlan;

import java.util.Locale;

import dji.common.flightcontroller.DJIGPSSignalStatus;
import dji.common.flightcontroller.DJILocationCoordinate3D;
import dji.common.flightcontroller.DJISimulatorInitializationData;
import dji.common.product.Model;
import dji.sdk.base.DJIBaseProduct;
import dji.sdk.camera.DJICamera;
import dji.sdk.codec.DJICodecManager;
import dji.sdk.flightcontroller.DJIFlightController;
import dji.sdk.flightcontroller.DJISimulator;
import dji.sdk.missionmanager.DJIWaypointMission;
import dji.sdk.products.DJIAircraft;

import static com.us.ifp.fpa.R.id.editText_numberPoints;


public class MainActivity extends AppCompatActivity
                          implements CreateMissionDialogFragment.NoticeDialogListener,
                                     StartSimulationDialogFragment.NoticeDialogListenerStartSim,
                                     GoHomeDialogFragment.NoticeDialogListenerGoHome,
                                     MissionControl.UpdateableActivityInterface,
                                     TextureView.SurfaceTextureListener {



    private static final String LOG_TAG = MainActivity.class.getName();

    private static final String PREFS_NAME       = "MainActivityPrefsFile";
    private static final String PREF_STARTPOINT  = "StartPoint";
    private static final String PREF_CURFPID     = "CurFpID";
    private static final String PREF_CURFPNAME   = "CurFpName";


    private String     mNotification;
    protected DJICamera.CameraReceivedVideoDataCallback mReceivedVideoDataCallBack = null;
    protected TextureView mVideoSurface = null;
    // Codec for video live view
    protected DJICodecManager mCodecManager = null;


    public MissionControl missionControl;

    private boolean mVideo_FS = false;

    // Last Point saved to Database:
    private int Pointhelp;



    public void switchVideo_FS() {
        if (mVideo_FS)
            setVideo_FS(false);
        else
            setVideo_FS(true);

    }

    public void setVideo_FS(boolean video_FS) {
        mVideo_FS = video_FS;
        View videoFS_view       = findViewById(R.id.textureView_liveCam_fs);
        View stateInfo_scrollView = findViewById(R.id.ScrollView_Infos);

        if (mVideo_FS) {
            if (videoFS_view != null)
                videoFS_view.setVisibility(View.VISIBLE);
            if (stateInfo_scrollView != null)
                stateInfo_scrollView.setVisibility(View.GONE);
        } else {
            if (videoFS_view != null)
                videoFS_view.setVisibility(View.GONE);
            if (stateInfo_scrollView != null)
                stateInfo_scrollView.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public String getNotification() {
        return mNotification;
    }
    @Override
    public void   setNotification(String notification) {
        mNotification = notification;
    }

    @Override
    public long getCurrentFlightPlanID() {
        SharedPreferences settings = this.getSharedPreferences(this.PREFS_NAME, 0);
        return settings.getLong(PREF_CURFPID, -1);
    }
    @Override
    public String getCurrentFlightPlanName() {
        SharedPreferences settings = this.getSharedPreferences(this.PREFS_NAME, 0);
        return settings.getString(PREF_CURFPNAME, "");
    }
    @Override
    public void updateMissionState() {
        Fragment fragment = getFragmentManager().findFragmentById(R.id.fragment_state_info);
        try {
            ((StateInfoFragment) fragment).updateMissionStateInfo();
        } catch (ClassCastException e) {

        }

    }




    @Override
    // TODO: updating the databse should be done in the MissionControl, not in this activity!
    public void WaypointMissionProgressStatusCallback(DJIWaypointMission.DJIWaypointMissionStatus djiWaypointMissionStatus) {

        // get settings for previously set StartPoint:
        SharedPreferences settings = this.getSharedPreferences(this.PREFS_NAME, 0);
        int  startPoint = settings.getInt(PREF_STARTPOINT, -1);
        long curFpID    = settings.getLong(PREF_CURFPID, -1);

        if(startPoint != -1 && curFpID != -1) {

            // Finished Point is nextPoint -1.
            // Because I add 2 extra Waypoints at the beginning, subtract 2.
            // Because WaypointIndex starts at 0 and the Flightplan starts at 1, add 1
            // -1 -2 +1 = -2; -2 +1 = -1
            int nextPoint     = djiWaypointMissionStatus.getTargetWaypointIndex() -1;
            int finishedPoint = nextPoint -1;



            // Not saving before first "real" Waypoint has been finished or after "real" Waypoints
            // are finsihed.
            if(finishedPoint >= 1 && finishedPoint < ((DJIWaypointMission) missionControl.getCurrentExecutingMission()).getWaypointCount()-3 ) {
                // When drone begins action at the next point, last point has been finished.
                // It would be more correct to do this at FinishedActionS, but this state doesn't
                // exist. And Writing to DB at Moving state would be efficient.
                // But BeginAction happens were rarely if any. And there can be Waypoints without
                // any actions. But I also want so save the Progress when mission is paused,
                // therefore I don't use the ExecutionState anymore.
                //  if(djiWaypointMissionStatus.getExecutionState() == DJIWaypointMission.DJIWaypointMissionExecutionState.Moving) {

                    // trigger only when really new update:
                    if(Pointhelp != finishedPoint+startPoint-1) {
                        Pointhelp = finishedPoint + startPoint-1;
                        // save finishedPoint to database:
                        ((FPAApplication) getApplication()).globalFlightPlanDBWorker.updateNumberOfFinishedWaypoints(curFpID, finishedPoint + startPoint-1);
                    }
                }
            // }
            //mNotification = "";
        } else {
            mNotification = "Warning: cannot save Mission Progress!";
            Log.w(LOG_TAG,"StartPoint from SharedPreferences is -1 or Current FlightPlanID is -1");
        }
    }




    @Override
    public void onGoHomeDialogPositiveClick(DialogFragment dialogFragment) {

        // get the dialog of the dialogfragment in which all the info is:
        Dialog dialog = dialogFragment.getDialog();

        // get the input:
        String goHomeAltitude_text  = ((EditText) dialog.findViewById(R.id.editText_GoHomeAltitude)).getText().toString();

        // Parsing input. Should work without Error because EditTexts have corresponding inputType
        final float goHomeAltitude  = Float.parseFloat(goHomeAltitude_text);

        missionControl.goHome(goHomeAltitude);
    }

    @Override
    public void onGoHomeDialogNegativeClick(DialogFragment dialog) {
    }




    @Override
    public void onStartSimDialogPositiveClick(DialogFragment dialogFragment) {


        // get the dialog of the dialogfragment in which all the info is:
        Dialog dialog = dialogFragment.getDialog();


        // get the input:
        String homePointLatitude_text  = ((EditText) dialog.findViewById(R.id.editText_homePointLatitude)).getText().toString();
        String homePointLongitude_text = ((EditText) dialog.findViewById(R.id.editText_homePointLongitude)).getText().toString();
        String SimSatCount_text        = ((EditText) dialog.findViewById(R.id.editText_SimSatCount)).getText().toString();


        // Parsing input. Should work without Error because EditTexts have corresponding inputType
        final double homePointLatitude  = Double.parseDouble(homePointLatitude_text);
        final double homePointLongitude = Double.parseDouble(homePointLongitude_text);
        final int    SimSatCount        = Integer.parseInt(SimSatCount_text);


        // get the Simulator instance:
        DJIBaseProduct mProduct   = FPAApplication.getProductInstance();
        DJISimulator djiSimulator = ((DJIAircraft) mProduct).getFlightController().getSimulator();


        // Start the Simulator:
        DJISimulatorInitializationData initializationData = new DJISimulatorInitializationData(homePointLatitude,homePointLongitude,20,SimSatCount);
        missionControl.startSimulator(initializationData);
    }

    @Override
    public void onStartSimDialogNegativeClick(DialogFragment dialogFragment) {

    }



    // The dialog fragment receives a reference to this Activity through the
    // Fragment.onAttach() callback, which it uses to call the following methods
    // defined by the NoticeDialogFragment.NoticeDialogListenerStartSim interface
    @Override
    public void onCreateMissionDialogPositiveClick(DialogFragment dialogFragment) {
        // User touched the dialog's positive button


        // get the dialog of the dialogfragment in which all the info is:
        Dialog dialog = dialogFragment.getDialog();


        // get the input:
        String fp_ID_text          = ((TextView) dialog.findViewById(R.id.textView_FP_ID)).getText().toString();
        String startPoint_text     = ((EditText) dialog.findViewById(R.id.editText_startPoint)).getText().toString();
        String numberPoints_text   = ((EditText) dialog.findViewById(editText_numberPoints)).getText().toString();
        String takeOffHeight_text  = ((EditText) dialog.findViewById(R.id.editText_takeOffHeight)).getText().toString();
        String goHomeAltitude_text = ((EditText) dialog.findViewById(R.id.editText_returnAltitude)).getText().toString();
        String northOffset_text    = ((EditText) dialog.findViewById(R.id.editText_northOffset)).getText().toString();
        String eastOffset_text     = ((EditText) dialog.findViewById(R.id.editText_eastOffset)).getText().toString();


        // Parsing input. Should work without Error because EditTexts have corresponding inputType
        long   fp_id          = Long.parseLong(fp_ID_text);
        int    startPoint     = Integer.parseInt(startPoint_text);
        int    numberOfPoints = Integer.parseInt(numberPoints_text);
        double takeOffHeight  = Double.parseDouble(takeOffHeight_text);
        double goHomeAltitude = Double.parseDouble(goHomeAltitude_text);
        double northOffset    = Double.parseDouble(northOffset_text);
        double eastOffset     = Double.parseDouble(eastOffset_text);


        // get the flightplan from the database:
        FlightPlan currentFlightPlan = ((FPAApplication) getApplication()).globalFlightPlanDBWorker.getFlightPlanByID(fp_id);


        // get current Position:
        DJIFlightController fc   = ((DJIAircraft) FPAApplication.getProductInstance()).getFlightController();
        DJILocationCoordinate3D currPos = fc.getCurrentState().getAircraftLocation();

        // Test for GPS Signal. Only allow setting up Mission, if "HomePoint" can be set correctly
        if(currPos == null || fc.getCurrentState().getGpsSignalStatus().value()  <= DJIGPSSignalStatus.Level1.value()) { // don't know the real behaviour, so I'm testing for both
            showToast("GPS Signal too low to get starting Coordinates",Toast.LENGTH_SHORT, "CreateMission", MCLogger.LogType.e);
            return;
        }


        // save startPoint in shared preferences in case app is closed during execution of the
        // mission. In that case saving the progress of the mission to the database can be
        // continued.
        SharedPreferences settings = this.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(PREF_STARTPOINT, startPoint);
        editor.putLong(PREF_CURFPID, fp_id);
        editor.putString(PREF_CURFPNAME, currentFlightPlan.properties.getPropertyByNameAsString("name"));
        editor.commit();


        // prepare:
        Pointhelp = -10;



        // convert North-/East-Offset[m] to Latitude-/Longitude-Offset[°]:
        double[] dLL = GeodeticToolkit.dXY2dLL(northOffset,eastOffset,currPos.getLatitude());
        double dLatitude  = dLL[0];
        double dLongitude = dLL[1];


        // create WayPointMission:
        DJIWaypointMission mission = currentFlightPlan.genereateWaypointMissionWithGoHomeAltitudeAndStartEndHoverOverHomePlus3m(currPos.getLatitude(),currPos.getLongitude(),startPoint,numberOfPoints,takeOffHeight,goHomeAltitude, dLatitude, dLongitude);


        String additionalData = String.format(Locale.US,"FPID: %d, FPName: %s, StartPoint: %d, NumberOfPoints: %d, TakeOffHeight[m]: %.2f, GoHomeAltitude[m]: %.2f .",
                fp_id, currentFlightPlan.properties.getPropertyByNameAsString("name"), startPoint, numberOfPoints,takeOffHeight,goHomeAltitude);

        showToast("Mission generated. Starting preparation...",Toast.LENGTH_SHORT, "CreateMission", MCLogger.LogType.i, additionalData);

        // prepare mission (uploading etc):
        missionControl.prepareMission(mission);


    }



    @Override
    public void onCreateMissionDialogNegativeClick(DialogFragment dialog) {
        // User touched the dialog's negative button
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Thread.setDefaultUncaughtExceptionHandler(new TopExceptionHandler(this));

        ((FPAApplication) getApplication()).setmCurrentActivity(this);

        // mission control has to be initialized before setting the contentView, because the
        // fragments are accessing it!
        missionControl = ((FPAApplication) getApplication()).getMissionControl();
        missionControl.setActivity(this);

        mNotification = "";

        setContentView(R.layout.activity_main);


        initUI();

        // The callback for receiving the raw H264 video data for camera live view
        mReceivedVideoDataCallBack = new DJICamera.CameraReceivedVideoDataCallback() {

            @Override
            public void onResult(byte[] videoBuffer, int size) {
                if(mCodecManager != null){
                    // Send the raw H264 video data to codec manager for decoding
                    mCodecManager.sendDataToDecoder(videoBuffer, size);
                }else {
                    Log.d(LOG_TAG, "mCodecManager is null");
                }
            }
        };

        DJICamera camera = missionControl.getCamera();


    }

    protected void onProductChange() {
        initPreviewer();
    }

    @Override
    public void onResume() {
        Log.v(LOG_TAG, "onResume");
        super.onResume();
        initPreviewer();
        onProductChange();

        if(mVideoSurface == null) {
            Log.d(LOG_TAG, "mVideoSurface is null");
        }
    }

    @Override
    public void onPause() {
        Log.v(LOG_TAG, "onPause");
        uninitPreviewer();
        super.onPause();
    }


    private void initUI() {
        // init mVideoSurface
        mVideoSurface = (TextureView)findViewById(R.id.textureView_liveCam_fs);

        if (null != mVideoSurface) {
            mVideoSurface.setSurfaceTextureListener(this);
            setVideo_FS(mVideo_FS);
        }

    }



    private void initPreviewer() {
        // TODO: in mein code-system umbauen
        DJIBaseProduct product = missionControl.getAircraft();

        if (product == null || !product.isConnected()) {
            showToast(getString(R.string.disconnected));
        } else {
            if (null != mVideoSurface) {
                mVideoSurface.setSurfaceTextureListener(this);
            }
            if (!product.getModel().equals(Model.UnknownAircraft)) {
                DJICamera camera = product.getCamera();
                if (camera != null){
                    // Set the callback
                    camera.setDJICameraReceivedVideoDataCallback(mReceivedVideoDataCallBack);
                }
            }
        }
    }

    private void uninitPreviewer() {
        DJICamera camera = missionControl.getCamera();
        if (camera != null){
            // Reset the callback
            missionControl.setDJICameraReceivedVideoDataCallback(null);
        }
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        Log.v(LOG_TAG, "onSurfaceTextureAvailable");
        if (mCodecManager == null) {
            mCodecManager = new DJICodecManager(this, surface, width, height);
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
        Log.v(LOG_TAG, "onSurfaceTextureSizeChanged");
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        Log.v(LOG_TAG,"onSurfaceTextureDestroyed");
        if (mCodecManager != null) {
            mCodecManager.cleanSurface();
            mCodecManager = null;
        }

        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {
    }





    public void showToast(final String msg) {
        runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
         //Important to unregister some callbacks:
         /* commented out, because onDestroy gets called when screen orientation is changed. Since I
         moved the mission controll into the application rather the MainActivity, I don't know
         where to call it otherwise. I just hope android can handle it automatically. */
        //missionControl.closeLog();
        //missionControl.close();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mainactivity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_offsettool) {
            startActivity(new Intent(this,OffsetToolActivity.class));
            return true;
        }

        if (id == R.id.action_settings) {
            startActivity(new Intent(this,SettingsListActivity.class));
            return true;
        }

        if (id == R.id.action_about) {
            startActivity(new Intent(this,AboutActivity.class));
            return true;
        }

        if (id == R.id.action_manageFlightPlans) {
            startActivity(new Intent(this,FlightPlanListActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    // Using this helper method to show Toasts, invoked by other threads.
    public void showToast(final String msg, int length, final String caller, final MCLogger.LogType logType) {
        if(length != Toast.LENGTH_LONG || length != Toast.LENGTH_SHORT) {
            length = Toast.LENGTH_SHORT;
        }
        final int lLength = length; // other thread => needs to be final;
        runOnUiThread(new Runnable() {
            @SuppressWarnings("WrongConstant")
            public void run() {
                Toast.makeText(getBaseContext(), msg, lLength).show();
                setNotification(msg);
                missionControl.log(caller + ": " + msg, logType);
                Log.i(LOG_TAG,msg);
            }
        });
    }

    // Using this helper method to show Toasts, invoked by other threads.
    public void showToast(final String msg, int length, final String caller, final MCLogger.LogType logType, final String additionalData) {
        if(length != Toast.LENGTH_LONG || length != Toast.LENGTH_SHORT) {
            length = Toast.LENGTH_SHORT;
        }
        final int lLength = length; // other thread => needs to be final;
        runOnUiThread(new Runnable() {
            @SuppressWarnings("WrongConstant")
            public void run() {
                Toast.makeText(getBaseContext(), msg, lLength).show();
                setNotification(msg);
                missionControl.log(caller + ": " + msg + ". " + additionalData, logType);
                Log.i(LOG_TAG,msg);
            }
        });
    }


}
