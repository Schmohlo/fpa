package com.us.ifp.fpa;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.os.CancellationSignal;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.us.ifp.fpa.MissionControl.MCLogger;
import com.us.ifp.fpa.MissionControl.MissionControl;
import com.us.ifp.fpa.MissionControl.MissionControlInterface;

import dji.common.error.DJIError;
import dji.common.util.DJICommonCallbacks;
import dji.sdk.base.DJIBaseProduct;
import dji.sdk.flightcontroller.DJISimulator;
import dji.sdk.products.DJIAircraft;


/** Important!!!
 * to work correctly, the Buttons in this Fragment are updated by the FlightControler
 * UpdateSystemStateCallback defined in StateInfoFragment!
 * I know this is ugly, but at this point I have no better idea.
 * A could awoid this if there would be working callback-functions for that Problem...
 */


public class ControlFragment extends    Fragment
                             implements View.OnClickListener{

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String LOG_TAG = ControlFragment.class.getName();


    // get the DJI Product, which was initialized by the Application class
    public DJIBaseProduct mProduct = FPAApplication.getProductInstance();



    private static final int GET_FP_REQUEST_CODE = 12345;

    public MissionControlInterface mMissionControl;




    public ControlFragment() {
        // Required empty public constructor
    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {


        if (requestCode == GET_FP_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if(resultData != null) {
                long id = resultData.getLongExtra(FlightPlanListFragment.EXTRA_RETURN_ID,-1);

                // create Dialog for inputs to create a mission:
                DialogFragment dialogFragment = new CreateMissionDialogFragment();
                Bundle args = new Bundle();
                args.putLong(CreateMissionDialogFragment.ARGUMENT_ID, id);
                dialogFragment.setArguments(args);
                dialogFragment.show(getActivity().getFragmentManager(), "");

            }

        }
    }




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment:
        View rootView = inflater.inflate(R.layout.fragment_control, container, false);


        Button mSSSim_Button   = (Button) rootView.findViewById(R.id.button_SSSim);
        Button mTaOf_Button    = (Button) rootView.findViewById(R.id.button_TakeOff);
        Button mLand_Button    = (Button) rootView.findViewById(R.id.button_Land);
        Button mGoHome_Button  = (Button) rootView.findViewById(R.id.button_GoHome);
        Button mSetHome_Button = (Button) rootView.findViewById(R.id.button_SetHome);
        Button mLoadM_Button   = (Button) rootView.findViewById(R.id.button_LoadMission);
        Button mStartM_Button  = (Button) rootView.findViewById(R.id.button_StartMission);
        Button mPauseM_Button  = (Button) rootView.findViewById(R.id.button_PauseMission);
        Button mResumeM_Button = (Button) rootView.findViewById(R.id.button_ResumeMission);
        Button mStopM_Button   = (Button) rootView.findViewById(R.id.button_StopMission);
        Button mVideoFS_Button = (Button) rootView.findViewById(R.id.button_videoFS);
        mSSSim_Button.setOnClickListener(this);
        mTaOf_Button.setOnClickListener(this);
        mLand_Button.setOnClickListener(this);
        mGoHome_Button.setOnClickListener(this);
        mSetHome_Button.setOnClickListener(this);
        mLoadM_Button.setOnClickListener(this);
        mStartM_Button.setOnClickListener(this);
        mPauseM_Button.setOnClickListener(this);
        mResumeM_Button.setOnClickListener(this);
        mStopM_Button.setOnClickListener(this);
        mVideoFS_Button.setOnClickListener(this);

        if(mProduct instanceof DJIAircraft) {

            mLand_Button.setText(R.string.land);
            mTaOf_Button.setText(R.string.takeOff);

            if (mMissionControl.hasSimulatorStarted()) {
                mSSSim_Button.setText(R.string.simStop);
            } else {
                mSSSim_Button.setText(R.string.simStart);
            }
        }

        /* In case there has been no StatusUpdateCallback set for the FlightControler (StateInfo-
         * Fragment covers this class as well), e.g. this Fragment is on its own, register a
         * StatusUpdateCallback.
         */
        // there is no way of finding out if there has already a Callback been set....

        return rootView;
    }






    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.button_videoFS: {
                try {
                    MainActivity activity = (MainActivity) getActivity();
                    activity.switchVideo_FS();
                } catch (ClassCastException e) {
                    // do nothing, because optional depending on activity;
                }
                break;
            }

            case R.id.button_StartMission: {
                mMissionControl.startMissionExecution();
                break;
            }

            case R.id.button_PauseMission: {
                mMissionControl.pauseMissionExecution();
                break;
            }

            case R.id.button_ResumeMission: {
                mMissionControl.resumeMissionExecution();
                break;
            }

            case R.id.button_StopMission: {

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                // Add the buttons
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mMissionControl.stopMissionExecution();
                    }
                });
                builder.setNegativeButton(R.string.cancel, null);
                // Chain together various setter methods to set the dialog characteristics
                builder.setMessage("Stop Mission ?")
                        .setTitle(getString(R.string.confirm) + " Stopping Mission");

                // Create the AlertDialog
                AlertDialog dialog = builder.create();
                dialog.show();
                break;
            }


            case R.id.button_LoadMission: {

                Intent intent = new Intent(getContext(),FlightPlanListActivity.class);
                intent.putExtra(FlightPlanListFragment.EXTRA_ENABLE_SELECTION,true);
                startActivityForResult(intent,this.GET_FP_REQUEST_CODE);

                break;
            }

            case R.id.button_SetHome: {

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                // Add the buttons
                builder.setPositiveButton("Set Home", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mMissionControl.setHomeLocationUsingAircraftCurrentLocation();
                    }
                });
                builder.setNegativeButton(R.string.cancel, null);
                // Chain together various setter methods to set the dialog characteristics
                builder.setMessage("Set HomePoint to current Aircraft-Location" + "?")
                        .setTitle(getString(R.string.confirm) + " " + "Set Home");

                // Create the AlertDialog
                AlertDialog dialog = builder.create();
                dialog.show();


                break;
            }

            case R.id.button_TakeOff: {

                if(!mMissionControl.isFlying() ) {
                    if (!mMissionControl.isTakingOff()) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        // Add the buttons
                        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                mMissionControl.takeOff();
                            }
                        });
                        builder.setNegativeButton(R.string.cancel, null);
                        // Chain together various setter methods to set the dialog characteristics
                        builder.setMessage(getString(R.string.takeOff) + "?")
                                .setTitle(getString(R.string.confirm) + " " + getString(R.string.takeOff));

                        // Create the AlertDialog
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    } else {
                        mMissionControl.cancelTakeOff();
                    }
                }
                break;
            }

            case R.id.button_GoHome: {

                if(mMissionControl.isFlying() && ((TextView) getActivity().findViewById
                        (R.id.button_Land)).getText() != getString(R.string.land_Cancel)) {
                    if (((TextView) view).getText().equals(getString(R.string.goHome))) {

                        // set goHome Altitude from dialog. Result handled by activity
                        DialogFragment dialogFragment = new GoHomeDialogFragment();
                        dialogFragment.show(getActivity().getFragmentManager(), "");

                    } else {
                        mMissionControl.cancelGoingHome();
                    }
                }
                break;
            }

            case R.id.button_Land: {

                if(mMissionControl.isFlying() && ((TextView) getActivity().findViewById
                        (R.id.button_GoHome)).getText() != getString(R.string.goHome_Cancel)) {
                    if (((TextView) view).getText().equals(getString(R.string.land))) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        // Add the buttons
                        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                mMissionControl.autoLand();
                            }
                        });
                        builder.setNegativeButton(R.string.cancel, null);
                        // Chain together various setter methods to set the dialog characteristics
                        builder.setMessage(getString(R.string.land) + "?")
                                .setTitle(getString(R.string.confirm) + " " + getString(R.string.land));

                        // Create the AlertDialog
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    } else {
                        mMissionControl.cancelAutoLanding();
                    }
                }
                break;
            }

            case R.id.button_SSSim: {

                DJISimulator djiSimulator = mMissionControl.getAircraft().getFlightController().getSimulator();

                // If simulator is off, turn it on
                if (!mMissionControl.hasSimulatorStarted()) {

                    // create Dialog for inputs to start simulation:
                    DialogFragment dialogFragment = new StartSimulationDialogFragment();
                    dialogFragment.show(getActivity().getFragmentManager(), "");


                } else { // if simulator is on and shell be turned off
                    mMissionControl.stopSimulator();
                }

                break;
            }
            default:
                break;


        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
          // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListenerStartSim so we can send events to the host
            mMissionControl = ((MainActivity) activity).missionControl;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement MissionControlStateInterface");
        }
    }



}
