package com.us.ifp.fpa.flightplanstuff;

import com.us.ifp.fpa.flightplanstuff.FlightPlanException;

/**
 * Created by V582 on 13.12.2016.
 */

public class FlightPlanParseException extends FlightPlanException {


    public FlightPlanParseException() {

    }

    public FlightPlanParseException(String s) {
        super(s);
    }

}
