package com.us.ifp.fpa;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.us.ifp.fpa.MissionControl.MissionControl;
import com.us.ifp.fpa.flightplanstuff.FlightPlanDBWorker;

import dji.common.error.DJIError;
import dji.common.error.DJISDKError;
import dji.sdk.base.DJIBaseComponent;
import dji.sdk.base.DJIBaseProduct;
import dji.sdk.camera.DJICamera;
import dji.sdk.products.DJIAircraft;
import dji.sdk.products.DJIHandHeld;
import dji.sdk.sdkmanager.DJISDKManager;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

/**
 * Created by V582 on 22.11.2016.
 */

/** Why do we use an Application class?
 * Because this class can handle tasks over several activities I think.
 */

public class FPAApplication extends Application {

    public static final String FLAG_CONNECTION_CHANGE = "fpv_tutorial_connection_change";

    private static DJIBaseProduct mProduct;

    private  MissionControl mMissionControl = null;

    private Handler mHandler;

    private Activity mCurrentActivity;



    // Use this worker to communicate with the Flight Plan Database.
    /* Laut bisherigem Kenntnissstand wird close automatisch aufgerufen, wenn app geschlossen
     * wird bzw. der DBHelper sorgt für consistenz, nachdem eine abfrage fertig ist.
     * Da die DB nur in dieser app und nur in diesem Thread aufgerufen wird, sollte das
     * funktionieren.
     */
    public FlightPlanDBWorker globalFlightPlanDBWorker;


    /* used to use the MissionControl instance from MainActivity outside of that Activity. Im not
     * putting the MC in the Application class because i want to ensure there is a drone attached
     * for the MC to work properly (which is the case, when MainActivity is entered). Maybe this
     * will be altered later. Know it is changed.
     */
    public MissionControl getMissionControl() {
        return mMissionControl;
    }
    public void setMissionControl(MissionControl missionControl) {
        mMissionControl = missionControl;
    }



    /**
     * This function is used to get the instance of DJIBaseProduct.
     * If no product is connected, it returns null.
     */
    public static synchronized DJIBaseProduct getProductInstance() {
        if (null == mProduct) {
            mProduct = DJISDKManager.getInstance().getDJIProduct();
        }
        return mProduct;
    }


    public static boolean isAircraftConnected() {
        return getProductInstance() != null && getProductInstance() instanceof DJIAircraft;
    }

    public static boolean isHandHeldConnected() {
        return getProductInstance() != null && getProductInstance() instanceof DJIHandHeld;
    }

    public static synchronized DJICamera getCameraInstance() {

        if (getProductInstance() == null) return null;

        DJICamera camera = null;

        if (getProductInstance() instanceof DJIAircraft){
            camera = ((DJIAircraft) getProductInstance()).getCamera();

        } else if (getProductInstance() instanceof DJIHandHeld) {
            camera = ((DJIHandHeld) getProductInstance()).getCamera();
        }

        return camera;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mHandler = new Handler(Looper.getMainLooper());
        //This is used to start SDK services and initiate SDK.
        DJISDKManager.getInstance().initSDKManager(this, mDJISDKManagerCallback);


        // Initialise DataBase worker for global use in this app.
        globalFlightPlanDBWorker = new FlightPlanDBWorker(this);
        globalFlightPlanDBWorker.open();


    }

    /**
     * When starting SDK services, an instance of interface DJISDKManager.DJISDKManagerCallback will be used to listen to
     * the SDK Registration result and the product changing.
     */
    private DJISDKManager.DJISDKManagerCallback mDJISDKManagerCallback = new DJISDKManager.DJISDKManagerCallback() {

        //Listens to the SDK registration result
        @Override
        public void onGetRegisteredResult(DJIError error) {

            if(error == DJISDKError.REGISTRATION_SUCCESS) {

                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Register Success", Toast.LENGTH_LONG).show();
                    }
                });

                DJISDKManager.getInstance().startConnectionToProduct();

            } else {

                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {

                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Register sdk fails, check network is available", Toast.LENGTH_LONG).show();
                    }
                });

            }
            Log.e("TAG", error.toString());
        }

        //Listens to the connected product changing, including two parts, component changing or product connection changing.
        @Override
        public void onProductChanged(DJIBaseProduct oldProduct, DJIBaseProduct newProduct) {

            mProduct = newProduct;
            if(mProduct != null) {
                mProduct.setDJIBaseProductListener(mDJIBaseProductListener);
            }

            notifyStatusChange();
        }
    };

    private DJIBaseProduct.DJIBaseProductListener mDJIBaseProductListener = new DJIBaseProduct.DJIBaseProductListener() {

        @Override
        public void onComponentChange(DJIBaseProduct.DJIComponentKey key, DJIBaseComponent oldComponent, DJIBaseComponent newComponent) {

            if(newComponent != null) {
                newComponent.setDJIComponentListener(mDJIComponentListener);
            }
            notifyStatusChange();
        }

        @Override
        public void onProductConnectivityChanged(boolean isConnected) {

            notifyStatusChange();

        }

    };

    private DJIBaseComponent.DJIComponentListener mDJIComponentListener = new DJIBaseComponent.DJIComponentListener() {

        @Override
        public void onComponentConnectivityChanged(boolean isConnected) {
            notifyStatusChange();
        }

    };

    private void notifyStatusChange() {
        mHandler.removeCallbacks(updateRunnable);
        mHandler.postDelayed(updateRunnable, 500);
        showToastWithTitle("Connection Status has changed.");
        Log.d("AppClass","notifyStatusChange");


        // Call ConnectionActivity to prevent crashes when replugging the UAV while MainActivity is
        // still on.
        // Check if ConnectionActivity is already started:
        if (!(getmCurrentActivity() instanceof ConnectionActivity)){

//            Intent intent = new Intent(this, ConnectionActivity.class);
//            Intent intent = Intent.makeMainActivity(new ComponentName("com.us.ifp.fpa","com.us.ifp.fpa.ConnectionActivity"));
            Intent intent = Intent.makeRestartActivityTask(new ComponentName("com.us.ifp.fpa","com.us.ifp.fpa.ConnectionActivity"));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK + Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    private Runnable updateRunnable = new Runnable() {
        @Override
        public void run() {
            // TODO: register every activity to receive the Broadcast.
            Intent intent = new Intent(FLAG_CONNECTION_CHANGE);
            sendBroadcast(intent);
        }
    };

    public void showToastWithTitle(String title) {
        Context applicationContext = getApplicationContext();
        if (applicationContext != null)
            Toast.makeText(applicationContext, title, Toast.LENGTH_SHORT).show();
    }

    public Activity getmCurrentActivity() {
        return mCurrentActivity;
    }

    public void setmCurrentActivity(Activity currentActivity) {
        this.mCurrentActivity = currentActivity;
    }
}
