package com.us.ifp.fpa;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.us.ifp.fpa.CrashReport.TopExceptionHandler;

import java.util.Locale;

import dji.common.error.DJIError;
import dji.common.util.DJICommonCallbacks;
import dji.sdk.flightcontroller.DJIFlightController;
import dji.sdk.products.DJIAircraft;

public class SettingsListActivity extends AppCompatActivity
                                  implements GoHomeDialogFragment.NoticeDialogListenerGoHome,
                                             CreateMissionDialogFragment.NoticeDialogListener{


    private final String LOG_TAG = SettingsListActivity.class.getSimpleName();



    // constant values for preferences etc:
    public static final String PREFS_NAME          = "GeneralPrefsFile";
    public static final String PREF_PNS            = "PlayNotificationSound";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_list);

        Thread.setDefaultUncaughtExceptionHandler(new TopExceptionHandler(this));
    }

    @Override
    public void onGoHomeDialogPositiveClick(DialogFragment dialogFragment) {
        // get the dialog of the dialogfragment in which all the info is:
        Dialog dialog = dialogFragment.getDialog();

        // get the input:
        String goHomeAltitude_text  = ((EditText) dialog.findViewById(R.id.editText_GoHomeAltitude)).getText().toString();

        // Parsing input. Should work without Error because EditTexts have corresponding inputType
        final float goHomeAltitude  = Float.parseFloat(goHomeAltitude_text);

        if(FPAApplication.getProductInstance() instanceof  DJIAircraft) {
            final DJIFlightController fc = ((DJIAircraft) FPAApplication.getProductInstance()).getFlightController();

                fc.setGoHomeAltitude(goHomeAltitude, new DJICommonCallbacks.DJICompletionCallback() {
                    @Override
                    public void onResult(DJIError djiError) {
                        if (djiError == null) {
                            showToast(String.format(Locale.GERMAN, "GoHomeAltitude set to %.2f", goHomeAltitude), Toast.LENGTH_SHORT);
                        } else {
                            showToast(djiError.getDescription(), Toast.LENGTH_LONG);
                        }
                    }
                });
        }

    }

    @Override
    public void onGoHomeDialogNegativeClick(DialogFragment dialog) {
    }

    @Override
    public void onCreateMissionDialogPositiveClick(DialogFragment dialog) {
    }

    @Override
    public void onCreateMissionDialogNegativeClick(DialogFragment dialog) {
    }


    // Using this helper method to show Toasts, invoked by other threads.
    public void showToast(final String msg, int length) {
        if(length != Toast.LENGTH_LONG || length != Toast.LENGTH_SHORT) {
            length = Toast.LENGTH_SHORT;
        }
        final int lLength = length; // other thread => needs to be final;
        runOnUiThread(new Runnable() {
            @SuppressWarnings("WrongConstant")
            public void run() {
                Toast.makeText(getBaseContext(), msg, lLength).show();
                Log.i(LOG_TAG,msg);
            }
        });
    }


}
