package com.us.ifp.fpa;

import android.app.DialogFragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class SettingsListFragment extends Fragment
                                  implements View.OnClickListener{



    private final String TAG = SettingsListFragment.class.getSimpleName();



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_settings_list, container, false);

        TextView goHome   = (TextView) rootView.findViewById(R.id.textview_goHomeAltitude_settingslistelement);
        TextView startM   = (TextView) rootView.findViewById(R.id.textview_startMission_settingslistelement);
        CheckBox playNS   = (CheckBox) rootView.findViewById(R.id.checkbox_playNotificationSound);
        goHome.setOnClickListener(this);
        startM.setOnClickListener(this);
        playNS.setOnClickListener(this);

        SharedPreferences settings = getActivity().getSharedPreferences(SettingsListActivity.PREFS_NAME, 0);
        if ( settings.getString(SettingsListActivity.PREF_PNS, "true").equals("true") ) {
            playNS.setChecked(true);
        } else {
            playNS.setChecked(false);
        }



        return  rootView;
    }




    @Override
    public void onClick(View view) {


        switch (view.getId()) {


            case R.id.checkbox_playNotificationSound: {

                // save preferences for next use:
                SharedPreferences settings = getActivity().getSharedPreferences(SettingsListActivity.PREFS_NAME, 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString(SettingsListActivity.PREF_PNS, String.valueOf(((CheckBox) view).isChecked()));
                editor.commit();
                break;
            }

            case R.id.textview_goHomeAltitude_settingslistelement: {
                // set goHome Altitude from dialog. Result handled by activity
                DialogFragment dialogFragment = new GoHomeDialogFragment();
                dialogFragment.show(getActivity().getFragmentManager(), "");
                break;
            }
            case R.id.textview_startMission_settingslistelement: {
                // set goHome Altitude from dialog. Result handled by activity
                DialogFragment dialogFragment = new CreateMissionDialogFragment();
                dialogFragment.show(getActivity().getFragmentManager(), "");
                break;
            }
        }
    }
}
