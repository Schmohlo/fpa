package com.us.ifp.fpa.CrashReport;

import android.util.Log;

import com.us.ifp.fpa.MissionControl.FPALogger;

import java.io.IOException;
import java.util.Calendar;
import java.util.Locale;

import dji.common.flightcontroller.DJIGPSSignalStatus;

/**
 * Created by V582 on 27.02.2017.
 */

public class CrashLogger extends FPALogger {

    // Constant strings:
    private static final String LOG_TAG = CrashLogger.class.getName();

    private static final String FILENAME_SUFFIX = "Crash";


    // format Strings:


    public CrashLogger() {
        super(FILENAME_SUFFIX);
    }

    @Override
    public void log(String logEntry) throws IOException {
        try {
            writeToLogFile(logEntry);
        } catch (IOException e) {
            throw e;
        }
    }

    @Override
    protected void writeHeadLine() throws IOException{

    }


}
