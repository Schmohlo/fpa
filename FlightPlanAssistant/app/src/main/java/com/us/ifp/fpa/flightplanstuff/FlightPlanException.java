package com.us.ifp.fpa.flightplanstuff;

/**
 * Created by V582 on 13.12.2016.
 */

public class FlightPlanException extends Exception {


    public FlightPlanException() {

    }

    public FlightPlanException(String s) {
        super(s);
    }

}
