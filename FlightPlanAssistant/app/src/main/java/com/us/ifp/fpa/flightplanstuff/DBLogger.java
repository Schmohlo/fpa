package com.us.ifp.fpa.flightplanstuff;

/**
 * Created by V582 on 03.03.2017.
 */

import android.util.Log;

import com.us.ifp.fpa.MissionControl.FPALogger;

import java.io.IOException;
import java.util.Calendar;
import java.util.Locale;

import dji.common.flightcontroller.DJIGPSSignalStatus;

/**
 * Created by V582 on 27.02.2017.
 */

public class DBLogger extends FPALogger {

    // Constant strings:
    private static final String LOG_TAG = DBLogger.class.getName();

    private static final String FILENAME_SUFFIX = "DataBase";
    private static final String HEADLINE        = "Timestamp, LogType: Message";


    // format Strings:
    private static final String LOG_ENTRY_MESSAGE_FORMAT     = "%s: %s";
    private static final String LOG_ENTRY_COMBINED_FORMAT    = ", " + LOG_ENTRY_MESSAGE_FORMAT;

    private static final String LOG_ENTRY_COMBINED_FORMAT_WITH_TIME = DATE_FORMAT.toPattern() + LOG_ENTRY_COMBINED_FORMAT;


    public enum LogType {
        i,w,e
    }



    public DBLogger() {
        super(FILENAME_SUFFIX);
    }

    @Override
    protected void writeHeadLine() throws IOException{
        String headerLines = HEADLINE + NEWLINE + LOG_ENTRY_COMBINED_FORMAT_WITH_TIME + NEWLINE;
        writeToLogFile(headerLines);
    }

    @Override
    public void log(String logEntry) throws IOException {
        try {
            writeToLogFile(logEntry);
        } catch (IOException e) {
            throw e;
        }
    }

    public void log(FlightPlan.ReadReport readReport) throws IOException {

        // assemble log string:
        // timestamp:
        String timeString  = DATE_FORMAT.format(Calendar.getInstance().getTime());
        // final assemble:
        String message = readReport.checkMessage;
        message = message.replace("\n"," ");
        message = message.replace("Please check data below:", "");
        message = "FlightPlan added to DataBase: " + message;
        
        // use us locale for "." as decimal seperator
        String logEntry = String.format(Locale.GERMAN,"%s" + LOG_ENTRY_COMBINED_FORMAT + NEWLINE,
                timeString,LogType.i,message);

        try {
            log(logEntry);
            Log.i(LOG_TAG, logEntry);
        } catch (IOException e) {
            throw e;
        }
    }


}

