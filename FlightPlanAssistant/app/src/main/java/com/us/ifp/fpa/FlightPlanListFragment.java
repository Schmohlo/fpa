package com.us.ifp.fpa;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.us.ifp.fpa.flightplanstuff.FlightPlanDBWorker;


public class FlightPlanListFragment extends Fragment {


    // Tag für das Logging des Fragment-Lifecycle definieren
    private final String LOG_TAG = FlightPlanListFragment.class.getSimpleName();

    private FlightPlanDBWorker worker;

    public static final String EXTRA_ENABLE_SELECTION  = "EXTRA_ENABLE_SELECTION";
    public static final String EXTRA_RETURN_ID         = "EXTRA_RETURN_ID";


    private boolean enableSelection;


    public FlightPlanListFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Menü bekannt geben, dadurch kann unser Fragment Menü-Events verarbeiten
        setHasOptionsMenu(true);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
       // inflater.inflate(R.menu.menu_aktienlistefragment,menu);
    }


    public boolean renewList() {

        Cursor cursor = worker.getCursorToAllFlightPlansSimple();

        int[] to = new int[]  {
                R.id.itemID_Data,
                R.id.itemName_Data,
                R.id.itemDateOfPlanning_Data,
                R.id.itemProgress_Data2,
                R.id.itemProgress_Data1,
                R.id.itemDateOfLoad_Data,
                R.id.itemDateOfLastChange_Data};

        String[] from = FlightPlanDBWorker.columnsSimple;

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(getActivity(),R.layout.list_item_flightplanlist,cursor,from, to,2  );

        ListView listView = (ListView) getActivity().findViewById(R.id.listview_FlightPlanList);
        if( listView != null ) {
            listView.setAdapter(adapter);
            return true;
        }
        return false;
    }


    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.menu_flight_plan_list_context, menu);
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
                .getMenuInfo();
        View viewElement = info.targetView;
        String text = ((TextView) viewElement.findViewById(R.id.itemID_Data)).getText().toString();

        final Long id_ = Long.parseLong(text);

        switch (item.getItemId()) {
            case R.id.action_deleteFlightPlan:
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                // Add the buttons
                builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        worker.deleteFlightPlanByID(id_);
                        renewList();
                    }
                });
                builder.setNegativeButton(R.string.cancel, null);
                builder.setMessage("Are you sure you want to delete this FlightPlan from the database?")
                        .setTitle(getString(R.string.confirm) + " deleting");
                AlertDialog dialog = builder.create();
                dialog.show();

                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Get the Intent by which the activity having this framgent was called.
        // Check if the intent is to select a FlightPlan (e.g. to create a Mission)
        Intent intent = getActivity().getIntent();
        if(intent != null && intent.hasExtra(this.EXTRA_ENABLE_SELECTION)) {
            if(intent.getBooleanExtra(this.EXTRA_ENABLE_SELECTION,false)) {
                this.enableSelection = true;
            } else {
                this.enableSelection = false;
            }
        }


        View rootView = (View) inflater.inflate(R.layout.fragment_flightplanlist, container, false);

        ListView listView = (ListView) rootView.findViewById(R.id.listview_FlightPlanList);
        registerForContextMenu(listView);

        if( this.enableSelection) {
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    String text_id = ((TextView) view.findViewById(R.id.itemID_Data)).getText().toString();
                    Long id_ = Long.parseLong(text_id);

                    Intent resultIntent = new Intent();

                    resultIntent.putExtra(EXTRA_RETURN_ID,id_);
                    getActivity().setResult(Activity.RESULT_OK, resultIntent);
                    getActivity().finish();

                }
            });
        }



        return  rootView;
    }

    public void onStart() {
        super.onStart();
        //worker = new FlightPlanDBWorker(getActivity().getApplicationContext());
       // worker.open();
        worker = ((FPAApplication) getActivity().getApplication()).globalFlightPlanDBWorker;
        renewList();
    }

    @Override
    public void onStop() {
        super.onStop();
       // worker.close();
    }

}

