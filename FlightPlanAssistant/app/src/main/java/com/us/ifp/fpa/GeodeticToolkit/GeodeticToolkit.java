package com.us.ifp.fpa.GeodeticToolkit;

/**
 * Created by V582 on 03.03.2017.
 */

public class GeodeticToolkit {

    public  static final double mRadius = 6371000;        // [m]
    private static final double mRho    = Math.PI / 180;  // [radian/°]

    private static final double mRxRho  = mRadius * mRho;



    public static double[] dLL2dXY(double dLatitude, double dLongitude, double Latitude) {

        double dX = dLatitude  * mRxRho;
        double dY = dLongitude * mRxRho * Math.cos(Latitude * mRho);

        return new double[] {dX, dY};
    }


    public static double[] dXY2dLL(double dNorth, double dEast, double Latitude) {

        double dLatitude  = dNorth / mRxRho;
        double dLongitude = dEast  / mRxRho / Math.cos(Latitude *mRho);

        return new double[] {dLatitude, dLongitude};
    }
}
