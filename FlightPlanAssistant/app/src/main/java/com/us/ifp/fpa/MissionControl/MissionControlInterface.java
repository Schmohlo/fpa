package com.us.ifp.fpa.MissionControl;

import dji.common.flightcontroller.DJIGPSSignalStatus;
import dji.common.flightcontroller.DJILocationCoordinate3D;
import dji.common.flightcontroller.DJISimulatorInitializationData;
import dji.sdk.camera.DJICamera;
import dji.sdk.flightcontroller.DJIFlightController;
import dji.sdk.flightcontroller.DJISimulator;
import dji.sdk.missionmanager.DJIMission;
import dji.sdk.products.DJIAircraft;

/**
 * Created by V582 on 07.02.2017.
 */

public interface MissionControlInterface {


    // "Totschlag"-getter: (should be discontinued / made private)
    public DJIAircraft         getAircraft();
    public DJIFlightController getFlightControler();
    public DJISimulator        getSimulator();


    // Camera Control:
    public void      setDJICameraReceivedVideoDataCallback(DJICamera.CameraReceivedVideoDataCallback cameraReceivedVideoDataCallback);
    public DJICamera getCamera();


    // Status Information:
    public DJIMission                          getCurrentExecutingMission();
    public DJIMission.DJIMissionProgressStatus getDJIMissionProgressStatus();
    public int                                 getProductEnergyRemainingPercent();
    public int                                 getRemoteControllerEnergyRemainingPercent();
    public boolean                             isFlying();
    public boolean                             hasSimulatorStarted();
    public String                              getMissionPreparationProgress();
    public String                              getMissionStateInfoText();
    public DJILocationCoordinate3D             getCurrentLocation();
    public DJIGPSSignalStatus                  getGPSSignalStatus();
    public double                              getAircraftHeading();
    public float                               getGimbalPitch();
    public boolean                             isGoingHome();
    public boolean                             isLanding();
    public boolean                             isTakingOff();


    // Commands:
    public void prepareMission(DJIMission djiMission);
    public void startMissionExecution();
    public void stopMissionExecution();
    public void pauseMissionExecution();
    public void resumeMissionExecution();
    public void takeOff();
    public void autoLand();
    public void goHome(final float goHomeAltitude);
    public void cancelAutoLanding();
    public void cancelGoingHome();
    public void cancelTakeOff();
    public void startSimulator(final DJISimulatorInitializationData initializationData);
    public void stopSimulator();
    public void setHomeLocationUsingAircraftCurrentLocation();



    // Logging:
    public void    setLastNotification(String lastNotification);
    public String  getLastNotification();
    public boolean log(String Message, MCLogger.LogType logType);
    public boolean openLog();
    public boolean closeLog();







}
