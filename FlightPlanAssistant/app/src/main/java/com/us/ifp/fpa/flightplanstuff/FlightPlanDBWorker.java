package com.us.ifp.fpa.flightplanstuff;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by V582 on 20.12.2016.
 */

public class FlightPlanDBWorker {
    private static final String LOG_TAG = FlightPlanDBWorker.class.getName();

    private SQLiteDatabase database;
    private FlightPlanDBHelper dbHelper;
    public  DBLogger logger;

    public static String[] columnsAll = {FlightPlanDBHelper.COLUMN_ID,
            FlightPlanDBHelper.COLUMN_NAME,
            FlightPlanDBHelper.COLUMN_DATEOFPLANNING,
            FlightPlanDBHelper.COLUMN_CENTRALPOINT,
            FlightPlanDBHelper.COLUMN_NUMBEROFWAYPOINTS,
            FlightPlanDBHelper.COLUMN_NUMBEROFWAYPOINTSWITHPICTURES,
            FlightPlanDBHelper.COLUMN_DESCRIPTION,
            FlightPlanDBHelper.COLUMN_NUMBEROFFINISHEDSTEPS,
            FlightPlanDBHelper.COLUMN_DATEOFLOAD,
            FlightPlanDBHelper.COLUMN_DATEOFLASTCHANGE,
            FlightPlanDBHelper.COLUMN_lINEDATASETS};

    public static String[] columnsSimple = {FlightPlanDBHelper.COLUMN_ID,
            FlightPlanDBHelper.COLUMN_NAME,
            FlightPlanDBHelper.COLUMN_DATEOFPLANNING,
            FlightPlanDBHelper.COLUMN_NUMBEROFWAYPOINTS,
            FlightPlanDBHelper.COLUMN_NUMBEROFFINISHEDSTEPS,
            FlightPlanDBHelper.COLUMN_DATEOFLOAD,
            FlightPlanDBHelper.COLUMN_DATEOFLASTCHANGE};




    public void updateNumberOfFinishedWaypoints(long ID, int NumberOfFinishedWaypoints) {

        ContentValues newValues = new ContentValues();

        newValues.put(FlightPlanDBHelper.COLUMN_NUMBEROFFINISHEDSTEPS, Integer.toString(NumberOfFinishedWaypoints));
        newValues.put(FlightPlanDBHelper.COLUMN_DATEOFLASTCHANGE, FlightPlan.calendarToString(Calendar.getInstance()));

        database.update(FlightPlanDBHelper.TABLE_FLIGHTPLANS, newValues, FlightPlanDBHelper.COLUMN_ID + " = " + Long.toString(ID), null);

    }


    public void writeFlightPlanToDB(FlightPlan flightPlan) {

        String lineDataSetsAsOneString = "";
        for (String dataSet: flightPlan.getLineDataSets()) {
            lineDataSetsAsOneString += dataSet + "\n";
        }
        lineDataSetsAsOneString = lineDataSetsAsOneString.trim();

        ContentValues values = new ContentValues();
        values.put(FlightPlanDBHelper.COLUMN_NAME,
                flightPlan.properties.getPropertyByNameAsString("name"));
        values.put(FlightPlanDBHelper.COLUMN_DATEOFPLANNING,
                flightPlan.properties.getPropertyByNameAsString("dateOfPlanning"));
        values.put(FlightPlanDBHelper.COLUMN_CENTRALPOINT,
                flightPlan.properties.getPropertyByNameAsString("centralPoint"));
        values.put(FlightPlanDBHelper.COLUMN_NUMBEROFWAYPOINTS,
                flightPlan.properties.getPropertyByNameAsString("numberOfWaypoints"));
        values.put(FlightPlanDBHelper.COLUMN_NUMBEROFWAYPOINTSWITHPICTURES,
                flightPlan.properties.getPropertyByNameAsString("numberOfWayPointsWithPictures"));
        values.put(FlightPlanDBHelper.COLUMN_DESCRIPTION,
                flightPlan.properties.getPropertyByNameAsString("description"));
        values.put(FlightPlanDBHelper.COLUMN_NUMBEROFFINISHEDSTEPS,
                flightPlan.getNumberOfFinishedSteps());
        values.put(FlightPlanDBHelper.COLUMN_DATEOFLOAD,
                flightPlan.getDateOfLoad());
        values.put(FlightPlanDBHelper.COLUMN_DATEOFLASTCHANGE,
                flightPlan.getDateOfLastChange());
        values.put(FlightPlanDBHelper.COLUMN_lINEDATASETS,
                lineDataSetsAsOneString);

        long insertId = database.insert(FlightPlanDBHelper.TABLE_FLIGHTPLANS, null, values);


        return;
    }


    public FlightPlan getFlightPlanByID(long ID) {

        Cursor cursor = getCursorToFlightPlanByID(ID);
        cursor.moveToFirst(); // don't know if necessary.
        return cursorToFlightPlan(cursor);
    }


    public boolean deleteFlightPlanByID(Long id) {

        return database.delete(FlightPlanDBHelper.TABLE_FLIGHTPLANS, FlightPlanDBHelper.COLUMN_ID + "=" + id.toString(), null) > 0;
    }


    public FlightPlan cursorToFlightPlan(Cursor cursor) {

        long id                   = cursor.getLong(  cursor.getColumnIndex(FlightPlanDBHelper.COLUMN_ID));
        int numberOfFinishedSteps = cursor.getInt(   cursor.getColumnIndex(FlightPlanDBHelper.COLUMN_NUMBEROFFINISHEDSTEPS));
        String dateOfLoad         = cursor.getString(cursor.getColumnIndex(FlightPlanDBHelper.COLUMN_DATEOFLOAD));
        String dateOfLastChange   = cursor.getString(cursor.getColumnIndex(FlightPlanDBHelper.COLUMN_DATEOFLASTCHANGE));
        String name               = cursor.getString(cursor.getColumnIndex(FlightPlanDBHelper.COLUMN_NAME));
        String dateOfPlanning     = cursor.getString(cursor.getColumnIndex(FlightPlanDBHelper.COLUMN_DATEOFPLANNING));
        String centralPoint       = cursor.getString(cursor.getColumnIndex(FlightPlanDBHelper.COLUMN_CENTRALPOINT));
        String numberOfWaypoints  = cursor.getString(cursor.getColumnIndex(FlightPlanDBHelper.COLUMN_NUMBEROFWAYPOINTS));
        String numberOfWayPointsWithPictures = cursor.getString(cursor.getColumnIndex(FlightPlanDBHelper.COLUMN_NUMBEROFWAYPOINTSWITHPICTURES));
        String description        = cursor.getString(cursor.getColumnIndex(FlightPlanDBHelper.COLUMN_DESCRIPTION));
        String lineDataSets       = cursor.getString(cursor.getColumnIndex(FlightPlanDBHelper.COLUMN_lINEDATASETS));


        FlightPlan flightPlan = new FlightPlan(id, numberOfFinishedSteps, dateOfLoad, dateOfLastChange,
                 name,  dateOfPlanning,  centralPoint,  numberOfWaypoints,
                 numberOfWayPointsWithPictures,  description, lineDataSets);

        return flightPlan;
    }


    public Cursor getCursorToAllFlightPlansSimple() {

        String orderBy = FlightPlanDBHelper.COLUMN_ID + " DESC";
        //orderBy = null;

        return database.query(FlightPlanDBHelper.TABLE_FLIGHTPLANS,
                columnsSimple, null, null, null, null, orderBy);
    }


    public Cursor getCursorToFlightPlanByID(long ID) {

        String where = FlightPlanDBHelper.COLUMN_ID + " = " + Long.toString(ID);

        return database.query(FlightPlanDBHelper.TABLE_FLIGHTPLANS,
                columnsAll, where, null, null, null, null);
    }


    public List<FlightPlan> getAllFlightPlans() {
        List<FlightPlan> flightPlanList = new ArrayList<>();

        Cursor cursor = database.query(FlightPlanDBHelper.TABLE_FLIGHTPLANS,
                columnsAll, null, null, null, null, null);


        cursor.moveToFirst();
        FlightPlan flightPlan;

        while(!cursor.isAfterLast()) {
            flightPlan = cursorToFlightPlan(cursor);
            flightPlanList.add(flightPlan);
            Log.d(LOG_TAG, "ID: " + flightPlan.getID() + ", Inhalt: " + flightPlan.properties.getPropertyByNameAsString("name"));
            cursor.moveToNext();
        }

        cursor.close();

        return flightPlanList;
    }



    public FlightPlanDBWorker(Context context) {
        Log.d(LOG_TAG, "Unsere DataSource erzeugt jetzt den dbHelper.");
        dbHelper = new FlightPlanDBHelper(context);
    }

    public void open() {
        Log.d(LOG_TAG, "Eine Referenz auf die Datenbank wird jetzt angefragt.");
        database = dbHelper.getWritableDatabase();
        Log.d(LOG_TAG, "Datenbank-Referenz erhalten. Pfad zur Datenbank: " + database.getPath());
        logger = new DBLogger();
        try {
            logger.open();
        } catch (IOException e) {
            Log.w(LOG_TAG,"Warning: can't create DatabaseLogfile");
        }
    }

    public void close() {
        dbHelper.close();
        Log.d(LOG_TAG, "Datenbank mit Hilfe des DbHelpers geschlossen.");
        try {
            logger.close();
        } catch (Exception e) {
            Log.w(LOG_TAG,"Warning: can't close DatabaseLogfile");
        }
    }
}