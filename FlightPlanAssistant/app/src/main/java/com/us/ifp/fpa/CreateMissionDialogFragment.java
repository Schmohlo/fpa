package com.us.ifp.fpa;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.us.ifp.fpa.flightplanstuff.FlightPlan;

import java.util.Locale;

import static android.view.View.GONE;


public class CreateMissionDialogFragment extends DialogFragment {

    private long fp_id;

    /* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    public interface NoticeDialogListener {
        public void onCreateMissionDialogPositiveClick(DialogFragment dialog);
        public void onCreateMissionDialogNegativeClick(DialogFragment dialog);
    }

    // Use this instance of the interface to deliver action events
    NoticeDialogListener mListener;

    /* Used to communicate, that the inputs are correct to the activity/fragment that called
       this dialog, so that there the Ok-Button can be set accordingly. */
    private boolean input_numberPoints_ok;
    private boolean input_startPoint_ok;

    // used to submit the fp_id of the flightplan to this dialog through argument bundle:
    public static final String ARGUMENT_ID = "ARGUMENT_ID";

    // constant values for preferences etc:
    public static final String PREFS_NAME          = "CreateMissionDialogPrefsFile";
    public static final String PREF_FP_NAME        = "FPName";
    public static final String PREF_STARTPOINT     = "StartPoint";
    public static final String PREF_NUMBERPOINTS   = "NumberPoints";
    public static final String PREF_RETURNALTITUDE = "ReturnAltitude";
    public static final String PREF_TAKEOFF_HEIGHT = "TakeOffHeight";
    public static final String PREF_NORTH_OFFSET   = "NorthOffset";
    public static final String PREF_EAST_OFFSET    = "EastOffset";

    // inner cass communication:
    private Button button_OK;



    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        // builder for convenient dialog building:
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());


        // Get the layout inflater:
        LayoutInflater inflater = getActivity().getLayoutInflater();


        // get the fp_id of the selected flightplan (was send through a argument bundle);
        int lmaxpoints = Integer.MAX_VALUE;
        FlightPlan flightPlan = null;
        if( getArguments() != null) {
            this.fp_id = getArguments().getLong(ARGUMENT_ID);

            // get flightplan from database:
            flightPlan = ((FPAApplication) getActivity().getApplication()).globalFlightPlanDBWorker.getFlightPlanByID(this.fp_id);
            lmaxpoints = Integer.parseInt(flightPlan.properties.getPropertyByNameAsString("numberOfWaypoints"));

        } else {
            this.fp_id = -1;
        }



        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View view = inflater.inflate(R.layout.dialog_fragment_create_mission, null);


        /* save EditTexts to static variables, so they can be accessed in the callback functions
        of the buttons and change listeners */
        final TextView textView_FP_Name        = ((TextView) view.findViewById(R.id.textView_FP_Name));
        final EditText editText_numberPoints   = ((EditText) view.findViewById(R.id.editText_numberPoints));
        final EditText editText_startPoint     = ((EditText) view.findViewById(R.id.editText_startPoint));
        final EditText editText_returnAltitude = ((EditText) view.findViewById(R.id.editText_returnAltitude));
        final EditText editText_takeOffHeight  = ((EditText) view.findViewById(R.id.editText_takeOffHeight));
        final EditText editText_northOffset    = ((EditText) view.findViewById(R.id.editText_northOffset));
        final EditText editText_eastOffset     = ((EditText) view.findViewById(R.id.editText_eastOffset));


        // Set Ok-Button text to save, when in settings menü. Otherwise it will say something like 'create mission'.
        String positiveButtonText = getString(R.string.CreateMission);
        if(getActivity() instanceof  SettingsListActivity) {
            positiveButtonText = getString(R.string.save);
        }

        // build the dialog (this has to be up here, so the OK-Button can be accessed in the
        // input change listener to enable/disable:
        builder.setView(view)
                // Add action buttons
                .setPositiveButton(positiveButtonText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        // save preferences for next use:
                        SharedPreferences settings = getActivity().getSharedPreferences(CreateMissionDialogFragment.PREFS_NAME, 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString(PREF_FP_NAME,        textView_FP_Name.getText().toString());
                        editor.putString(PREF_STARTPOINT,     editText_startPoint.getText().toString());
                        editor.putString(PREF_NUMBERPOINTS,   editText_numberPoints.getText().toString());
                        editor.putString(PREF_RETURNALTITUDE, editText_returnAltitude.getText().toString());
                        editor.putString(PREF_TAKEOFF_HEIGHT, editText_takeOffHeight.getText().toString());
                        editor.putString(PREF_NORTH_OFFSET,   editText_northOffset.getText().toString());
                        editor.putString(PREF_EAST_OFFSET,    editText_eastOffset.getText().toString());
                        editor.commit();

                        mListener.onCreateMissionDialogPositiveClick(CreateMissionDialogFragment.this);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        mListener.onCreateMissionDialogNegativeClick(CreateMissionDialogFragment.this);
                    }
                });


        // create dialog:
        AlertDialog dialog = builder.create();
        // dialog.show() so that the Buttons get initialized. This doesn't display the dialog
        // however.
        dialog.show();


        /* save TextViews, the OK-Button and the number of waypoints in this flight plan to static
           variables, so they can be accessed in the callback functions of the buttons and change
           listeners */
        final TextView textView_startPoint_Message   = ((TextView) view.findViewById(R.id.textView_startPoint_Message));
        final TextView textView_numberPoints_Message = ((TextView) view.findViewById(R.id.textView_numberPoints_Message));
        button_OK           = dialog.getButton(Dialog.BUTTON_POSITIVE);
        final int maxpoints = lmaxpoints;


        /* Mechanism for checking inputs. Important: this block of code has to be before setting
           default values, so that they can be checked and displayed as wrong, also. */
        ((EditText) view.findViewById(R.id.editText_startPoint)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                textView_startPoint_Message.setText("");
                textView_startPoint_Message.setVisibility(GONE);
                set_input_startPoint_ok(true);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = editable.toString();
                try {
                    int content = Integer.parseInt(text);
                    if (content < 1 || content > maxpoints) {
                        set_input_startPoint_ok(false);
                        textView_startPoint_Message.setVisibility(View.VISIBLE);
                        textView_startPoint_Message.setText(String.format(Locale.GERMAN, "Must be between 1 and %d", maxpoints));
                    }
                } catch (NumberFormatException e) {
                    set_input_startPoint_ok(false);
                    textView_startPoint_Message.setVisibility(View.VISIBLE);
                    textView_startPoint_Message.setText(String.format(Locale.GERMAN, "Not a valid integer (max %d)", Integer.MAX_VALUE));
                }
            }
        });
        ((EditText) view.findViewById(R.id.editText_numberPoints)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                textView_numberPoints_Message.setText("");
                textView_numberPoints_Message.setVisibility(GONE);
                set_input_numberPoints_ok(true);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = editable.toString();
                try {
                    int content = Integer.parseInt(text);

                    // 95 = 99-2-2!
                    if (content < 1 || content > 95) {
                        set_input_numberPoints_ok(false);
                        textView_numberPoints_Message.setVisibility(View.VISIBLE);
                        textView_numberPoints_Message.setText("Must be between 1 and 95!");
                    }
                } catch (NumberFormatException e) {
                    set_input_numberPoints_ok(false);
                    textView_numberPoints_Message.setVisibility(View.VISIBLE);
                    textView_numberPoints_Message.setText(String.format(Locale.GERMAN, "Not a valid integer (max %d)", Integer.MAX_VALUE));
                }
            }
        });


        // get settings for previously used input data:
        SharedPreferences settings = getActivity().getSharedPreferences(this.PREFS_NAME, 0);


        // set default values:
        if(flightPlan != null) {
            ((TextView) view.findViewById(R.id.textView_FP_Name)).setText(flightPlan.properties.getPropertyByNameAsString("name"));
            ((EditText) view.findViewById(R.id.editText_startPoint)).setText(Integer.toString(flightPlan.getNumberOfFinishedSteps() + 1));
        } else {
            ((TextView) view.findViewById(R.id.textView_FP_Name)).setText(settings.getString(PREF_FP_NAME, "NoFPSelected"));
            ((EditText) view.findViewById(R.id.editText_startPoint)).setText(settings.getString(PREF_STARTPOINT, "1"));
        }
        ((EditText) view.findViewById(R.id.editText_numberPoints)).setText(settings.getString(PREF_NUMBERPOINTS, "95"));
        ((EditText) view.findViewById(R.id.editText_returnAltitude)).setText(settings.getString(PREF_RETURNALTITUDE, "0"));
        ((EditText) view.findViewById(R.id.editText_takeOffHeight)).setText(settings.getString(PREF_TAKEOFF_HEIGHT, "0"));
        ((EditText) view.findViewById(R.id.editText_northOffset)).setText(settings.getString(PREF_NORTH_OFFSET, "0"));
        ((EditText) view.findViewById(R.id.editText_eastOffset)).setText(settings.getString(PREF_EAST_OFFSET, "0"));


        // set ID in corresponding TextView, so that it can be read when dialog answer is send:
        ((TextView) view.findViewById(R.id.textView_FP_ID)).setText(String.format("%d",this.fp_id));


        return dialog;
    }



    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListenerStartSim
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListenerStartSim so we can send events to the host
            mListener = (NoticeDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListenerStartSim");
        }
    }







    private void set_input_numberPoints_ok(boolean value) {
        input_numberPoints_ok = value;
        setOKButtonEnabled();
    }

    private void set_input_startPoint_ok(boolean value) {
        input_startPoint_ok = value;
        setOKButtonEnabled();
    }

    private void setOKButtonEnabled() {
        button_OK.setEnabled(inputOK());
    }

    private boolean inputOK() {
        return input_startPoint_ok && input_numberPoints_ok;
    }
}

