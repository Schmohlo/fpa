package com.us.ifp.fpa;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.us.ifp.fpa.GeodeticToolkit.GeodeticToolkit;
import com.us.ifp.fpa.MissionControl.MCLogger;
import com.us.ifp.fpa.MissionControl.MissionControl;

import java.util.Locale;

import dji.common.flightcontroller.DJILocationCoordinate3D;

import static java.lang.Float.NaN;


/**
 * A placeholder fragment containing a simple view.
 */
public class OffsetToolFragment extends Fragment
                                implements View.OnClickListener{


    private final String LOG_TAG = OffsetToolFragment.class.getSimpleName();

    private TextView textView_curLat;
    private TextView textView_curLon;

    private EditText editText_Lat;
    private EditText editText_Lon;
    private EditText editText_dNorth;
    private EditText editText_dEast;


    private double mCurrent_Lat = NaN;
    private double mCurrent_Lon = NaN;


    private MissionControl mMissionControl;

    private OffsetToolActivity mActivity;


    // constant values for preferences etc:
    public static final String PREFS_NAME          = "OffsetToolPrefsFile";
    public static final String PREF_LONGITUDE      = "Longitude";
    public static final String PREF_LATITUDE       = "Latitude";




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_offsettool, container, false);

        mActivity = (OffsetToolActivity) getActivity();

        textView_curLat = (TextView) rootView.findViewById(R.id.textView_CurPos_Lat_Data);
        textView_curLon = (TextView) rootView.findViewById(R.id.textView_CurPos_Lon_Data);

        editText_Lat    = (EditText) rootView.findViewById(R.id.editText_Lat);
        editText_Lon    = (EditText) rootView.findViewById(R.id.editText_Lon);
        editText_dNorth = (EditText) rootView.findViewById(R.id.editText_dNorth);
        editText_dEast  = (EditText) rootView.findViewById(R.id.editText_dEast);

        Button button_refresh = (Button) rootView.findViewById(R.id.button_Refresh);
        Button button_G2K     = (Button) rootView.findViewById(R.id.button_G2K);
        Button button_K2G     = (Button) rootView.findViewById(R.id.button_K2G);
        Button button_Submit  = (Button) rootView.findViewById(R.id.button_Submit);


        // set default values:
        SharedPreferences settings = getActivity().getSharedPreferences(this.PREFS_NAME, 0);
        editText_Lat.setText(settings.getString(PREF_LATITUDE, ""));
        editText_Lon.setText(settings.getString(PREF_LONGITUDE, ""));

        settings = getActivity().getSharedPreferences(CreateMissionDialogFragment.PREFS_NAME, 0);
        editText_dNorth.setText(settings.getString(CreateMissionDialogFragment.PREF_NORTH_OFFSET, ""));
        editText_dEast.setText(settings.getString(CreateMissionDialogFragment.PREF_EAST_OFFSET, ""));


        button_refresh.setOnClickListener(this);
        button_G2K.setOnClickListener(this);
        button_K2G.setOnClickListener(this);
        button_Submit.setOnClickListener(this);

        mMissionControl = ((FPAApplication) getActivity().getApplication()).getMissionControl();
        RefreshCurrentPosition();



        return  rootView;
    }




    public void RefreshCurrentPosition() {
        if (mMissionControl != null) {
            DJILocationCoordinate3D curPos = mMissionControl.getCurrentLocation();
            mCurrent_Lat = curPos.getLatitude();
            mCurrent_Lon = curPos.getLongitude();
        } else {
            mCurrent_Lat = NaN;
            mCurrent_Lon = NaN;
        }
        textView_curLat.setText(String.format(Locale.GERMAN, "%12.7f", mCurrent_Lat));
        textView_curLon.setText(String.format(Locale.GERMAN, "%12.7f", mCurrent_Lon));
    }



    @Override
    public void onClick(View view) {


        switch (view.getId()) {


            case R.id.button_G2K: {
                double dLat = 0;
                double dLon = 0;

                try {
                    dLat = mCurrent_Lat - Double.parseDouble(editText_Lat.getText().toString());
                } catch (NumberFormatException e){
                }
                try {
                    dLon = mCurrent_Lon - Double.parseDouble(editText_Lon.getText().toString());
                } catch (NumberFormatException e) {
                }

                double[] dXY = GeodeticToolkit.dLL2dXY(dLat, dLon, mCurrent_Lat);

                editText_dNorth.setText(String.format(Locale.US, "%.3f", dXY[0]));
                editText_dEast.setText(String.format(Locale.US, "%.3f", dXY[1]));

                String msg = String.format("actualLat[°]= %s, actualLon[°]= %s, targetLat[°]= %s, targetLon[°]= %s to dN[m]= %s, dE[m]= %s.",
                        textView_curLat.getText().toString(), textView_curLon.getText().toString(),
                        editText_Lat.getText().toString()   , editText_Lon.getText().toString(),
                        editText_dNorth.getText().toString(), editText_dEast.getText().toString());
                makeLog("OffsetTool_G2K",msg);

                break;
            }


            case R.id.button_K2G: {
                double dNorth = 0;
                double dEast  = 0;

                try {
                    dNorth = Double.parseDouble(editText_dNorth.getText().toString());
                } catch (NumberFormatException e) {
                }
                try {
                    dEast = Double.parseDouble(editText_dEast.getText().toString());
                } catch (NumberFormatException e) {
                }

                double[] dLL = GeodeticToolkit.dXY2dLL(dNorth, dEast, mCurrent_Lat);

                editText_Lat.setText(String.format(Locale.US, "%.9f", mCurrent_Lat - dLL[0]));
                editText_Lon.setText(String.format(Locale.US, "%.9f", mCurrent_Lon - dLL[1]));

                String msg = String.format("actualLat[°]= %s, actualLon[°]= %s, dN[m]= %s, dE[m]= %s to targetLat[°]= %s, targetLon[°]= %s.",
                        textView_curLat.getText().toString(), textView_curLon.getText().toString(),
                        editText_dNorth.getText().toString(), editText_dEast.getText().toString(),
                        editText_Lat.getText().toString()   , editText_Lon.getText().toString());
                makeLog("OffsetTool_K2G",msg);

                break;
            }



            case R.id.button_Refresh: {
                RefreshCurrentPosition();
                break;
            }


            case R.id.button_Submit: {
                double dNorth = 0;
                double dEast  = 0;

                try {
                    dNorth = Double.parseDouble(editText_dNorth.getText().toString());
                } catch (NumberFormatException e) {
                }
                try {
                    dEast = Double.parseDouble(editText_dEast.getText().toString());
                } catch (NumberFormatException e) {
                }

                double dLat = 0;
                double dLon = 0;

                try {
                    dLat = mCurrent_Lat - Double.parseDouble(editText_Lat.getText().toString());
                } catch (NumberFormatException e){
                }
                try {
                    dLon = mCurrent_Lon - Double.parseDouble(editText_Lon.getText().toString());
                } catch (NumberFormatException e) {
                }


                SharedPreferences settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString(PREF_LATITUDE,   dLat == 0? "" : editText_Lat.getText().toString());
                editor.putString(PREF_LONGITUDE,  dLon == 0? "" : editText_Lon.getText().toString());
                editor.commit();

                settings = getActivity().getSharedPreferences(CreateMissionDialogFragment.PREFS_NAME, 0);
                editor = settings.edit();
                editor.putString(CreateMissionDialogFragment.PREF_NORTH_OFFSET, dNorth == 0? "0" : editText_dNorth.getText().toString());
                editor.putString(CreateMissionDialogFragment.PREF_EAST_OFFSET,  dEast  == 0? "0" : editText_dEast.getText().toString());
                editor.commit();

                showToast("Values have been submitted to 'Load M' Dialog",
                        Toast.LENGTH_LONG,
                        "OffsetTool_submit",
                        MCLogger.LogType.i,
                        "dN[m]="+editText_dNorth.getText().toString()+" dE[m]="+editText_dEast.getText().toString()+".",true);

                break;
            }
        }
    }


    private void makeLog(String caller, String msg) {

        mMissionControl.log(caller + ": " + msg, MCLogger.LogType.i);
        Log.i(LOG_TAG,caller + ": " + msg);
    }



    // Using this helper method to show Toasts, invoked by other threads.
    public void showToast(final String msg, int length, final String caller, final MCLogger.LogType logType, final String additionalData, final Boolean showDialog) {
        if(length != Toast.LENGTH_LONG && length != Toast.LENGTH_SHORT) {
            length = Toast.LENGTH_SHORT;
        }
        final int lLength = length; // other thread => needs to be final;
        getActivity().runOnUiThread(new Runnable() {
            @SuppressWarnings("WrongConstant")
            public void run() {
                if ( showDialog) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    // Add the buttons
                    builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
                    // Chain together various setter methods to set the dialog characteristics
                    builder.setMessage(msg).setTitle(getString(R.string.notification));
                    // Create the AlertDialog
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                Toast.makeText(getActivity(), msg, lLength).show();
                mMissionControl.log(caller + ": " + msg + ". " + additionalData, logType);
                mMissionControl.setLastNotification(msg);
                Log.i(LOG_TAG,msg);
            }
        });
    }


}
