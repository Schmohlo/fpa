package com.us.ifp.fpa.MissionControl;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.us.ifp.fpa.GeodeticToolkit.GeodeticToolkit;
import com.us.ifp.fpa.MainActivity;
import com.us.ifp.fpa.R;
import com.us.ifp.fpa.SettingsListActivity;

import java.io.IOException;
import java.util.Locale;

import dji.common.battery.DJIBatteryState;
import dji.common.error.DJIError;
import dji.common.flightcontroller.DJIFlightControllerFlightMode;
import dji.common.flightcontroller.DJIGPSSignalStatus;
import dji.common.flightcontroller.DJILocationCoordinate2D;
import dji.common.flightcontroller.DJILocationCoordinate3D;
import dji.common.flightcontroller.DJISimulatorInitializationData;
import dji.common.remotecontroller.DJIRCBatteryInfo;
import dji.common.remotecontroller.DJIRCHardwareState;
import dji.common.util.DJICommonCallbacks;
import dji.sdk.battery.DJIBattery;
import dji.sdk.camera.DJICamera;
import dji.sdk.flightcontroller.DJIFlightController;
import dji.sdk.flightcontroller.DJISimulator;
import dji.sdk.missionmanager.DJIMission;
import dji.sdk.missionmanager.DJIMissionManager;
import dji.sdk.missionmanager.DJIWaypointMission;
import dji.sdk.products.DJIAircraft;
import dji.sdk.remotecontroller.DJIRemoteController;

/**
 * Created by V582 on 06.02.2017.
 */

public class MissionControl implements MissionControlInterface{



    private static String LOG_TAG = MissionControl.class.getName();

    private MCLogger mMCLogger;


    // basic member field #1:
    private        Activity            mActivity; // activity is used to show Toasts etc.
    // basic member field #2 :
    private static DJIAircraft         mDJIAircraft;

    // for convenience:
    private static DJIMissionManager                   mDJIMissionManager;

    // for access when needed, outside of callback:
    private static DJIMission.DJIMissionProgressStatus mDJIMissionProgressStatus;
    private static int                                 mProductEnergyRemainingPercent;
    private static int                                 mRemoteControllerEnergyRemainingPercent;
    private static String                              mMissionPreparationProgress;


    private static int EnergyRemainingPercentAtMissionStart;

    private double  mLastLongitude    = 0;
    private double  mLastLatitude     = 0;
    private float   mLastAltitude     = 0;
    private String  mLastMissionState = "";
    private String  mLastNotification = "";


    // Interface for activity to get called in WaypointMissionProgressStatusCallback:
    public interface UpdateableActivityInterface {
        public void   WaypointMissionProgressStatusCallback(DJIWaypointMission.DJIWaypointMissionStatus djiWaypointMissionStatus);
        public String getNotification();
        public void   setNotification(String notification);
        public long   getCurrentFlightPlanID();
        public String getCurrentFlightPlanName();
        public void   updateMissionState();
    }


    /**
     *
     * @param activity     current activity to show Toasts
     */
    public void setActivity( Activity activity) {

        // test if activity implements Interface. If not, an Exception is thrown.
        UpdateableActivityInterface test = (UpdateableActivityInterface) activity;

        mActivity = activity;
        LOG_TAG            = MissionControl.class.getName() + " in " + mActivity.getClass().getName();
    }


    /**
     * MissionControl Constructor initializes member variables and sets callbacks to update member
     * fields.
     *
     * @param djiAircraft  DJIProduct casted to DJIAircraft;
     */
    public MissionControl( DJIAircraft djiAircraft) {

        // Initialize member fields:
        mActivity          = null;
        LOG_TAG            = MissionControl.class.getName() + " in " + "no_activity_been_set_yet";
        mDJIAircraft       = djiAircraft;
        mDJIMissionManager = mDJIAircraft.getMissionManager();
        mMissionPreparationProgress = "";

        mMCLogger = new MCLogger();


        // Initialize callbacks to set own member fields
        mDJIMissionManager.setMissionProgressStatusCallback(new DJIMissionManager.MissionProgressStatusCallback() {
            @Override
            public void missionProgressStatus(DJIMission.DJIMissionProgressStatus djiMissionProgressStatus) {
                mDJIMissionProgressStatus = djiMissionProgressStatus;

                // logging:
                String missionState = getMissionStateInfoText();
                // log only, if new message OR Location changed at least 2m:
                double[] dXY = GeodeticToolkit.dLL2dXY(
                        getCurrentLocation().getLatitude()  - mLastLatitude,
                        getCurrentLocation().getLongitude() - mLastLongitude,mLastLatitude);

                if(!missionState.equals(mLastMissionState) || (Math.pow(dXY[0],2) + Math.pow(dXY[1],2) + Math.pow(mLastAltitude-getCurrentLocation().getAltitude(),2)) > 4) {

                    log("updateMissionStateInfo(): " + missionState, MCLogger.LogType.i);
                    mLastLatitude  = getCurrentLocation().getLatitude();
                    mLastLongitude = getCurrentLocation().getLongitude();
                    mLastAltitude  = getCurrentLocation().getAltitude();
                    mLastMissionState = missionState;
                }

                // call method in MainActivity  to update database
                CallWaypointMissionProgressStatusCallback(djiMissionProgressStatus);
            }
        });
        mDJIMissionManager.setMissionExecutionFinishedCallback(new DJICommonCallbacks.DJICompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                if(djiError == null) {
                    showToast("Mission completed. Waiting...",Toast.LENGTH_SHORT, "MissionExecutionFinishedCallback()", MCLogger.LogType.i,true);
                } else {
                    showToast(djiError.getDescription(),Toast.LENGTH_LONG, "MissionExecutionFinishedCallback()", MCLogger.LogType.e,true);
                }
            }
        });

        mDJIAircraft.getBattery().setBatteryStateUpdateCallback(new DJIBattery.DJIBatteryStateUpdateCallback() {
            @Override
            public void onResult(DJIBatteryState djiBatteryState) {
                mProductEnergyRemainingPercent = djiBatteryState.getBatteryEnergyRemainingPercent();
            }
        });
        mDJIAircraft.getRemoteController().setBatteryStateUpdateCallback(new DJIRemoteController.RCBatteryStateUpdateCallback() {
            @Override
            public void onBatteryStateUpdate(DJIRemoteController djiRemoteController, DJIRCBatteryInfo djircBatteryInfo) {
                mRemoteControllerEnergyRemainingPercent = djircBatteryInfo.remainingEnergyInPercent;
            }
        });

        // Activate Custom Buttons on Remote Control:
        mDJIAircraft.getRemoteController().setHardwareStateUpdateCallback(new DJIRemoteController.RCHardwareStateUpdateCallback() {
            @Override
            public void onHardwareStateUpdate(DJIRemoteController djiRemoteController, DJIRCHardwareState state) {
                if (state.customButton1.buttonDown) {
                    pauseMissionExecution();
                } else if (state.customButton2.buttonDown) {
                    resumeMissionExecution();
                }
            }
        });
    }


    // implemented as extra function, because the mActivity can be null or pointing at an old instance
    private void CallWaypointMissionProgressStatusCallback(DJIMission.DJIMissionProgressStatus djiMissionProgressStatus) {

        try {
            ((UpdateableActivityInterface)mActivity).WaypointMissionProgressStatusCallback((DJIWaypointMission.DJIWaypointMissionStatus) djiMissionProgressStatus);
        } catch (ClassCastException e) {
            // do nothing, cause only optional
        } catch (NullPointerException npe) {
            // do nothing, becauce mActivity could not be set, yet.
        }
    }

    /**
     * Because there is no destrutor in Java, I use finalaze(), which may be called by the gc,
     * or close() for explicit calls (preferred). This is important because the callbacks have to be
     * unregistered to prevent crashes or other unpredictable behaviour;
     */
    protected void finalize() {
        this.close();
    }
    public void close() {
        mDJIMissionManager.setMissionProgressStatusCallback(null);
        mDJIMissionManager.setMissionExecutionFinishedCallback(null);
        mDJIAircraft.getBattery().setBatteryStateUpdateCallback(null);
    }



    @Override
    public boolean log(String Message, MCLogger.LogType logType) {

        try {
            mMCLogger.log(Message, logType,
                    getCurrentLocation().getLatitude(),
                    getCurrentLocation().getLongitude(),
                    getCurrentLocation().getAltitude(),
                    getAircraftHeading(),
                    getGimbalPitch(),
                    isFlying(),
                    getGPSSignalStatus());
            return true;
        } catch (Exception e) {
            if (mActivity != null)
                setLastNotification(e.toString());
            return false;
        }
    }

    @Override
    public boolean openLog() {
        try {
            mMCLogger.open();
            return true;
        } catch (Exception e) {
            showToast("Warning: can't create MissionControleLogfile",Toast.LENGTH_SHORT, "openLog()", MCLogger.LogType.w,true);
            return false;
        }
    }
    @Override
    public boolean closeLog() {
        try {
            mMCLogger.close();
            return true;
        } catch (Exception e) {
            showToast("Warning: can't close MissionControleLogfile",Toast.LENGTH_SHORT, "closeLog()", MCLogger.LogType.w,true);
            return false;
        }
    }


    // implemented as extra function, because the mActivity can be null or pointing at an old instance
    private void callUpdateMissionState() {
        try {
            ((UpdateableActivityInterface)mActivity).updateMissionState();
        } catch (ClassCastException e) {
            // do nothing, cause only optional
        } catch (NullPointerException npe) {
            // do nothing, maybe activity has been set, yet.
        }
    }

    // Implementing Command Interfaces:
    @Override
    public void prepareMission(DJIMission djiMission) {

        mDJIMissionManager.prepareMission(djiMission,
                new DJIMission.DJIMissionProgressHandler() {
                    @Override
                    public void onProgress(DJIMission.DJIProgressType djiProgressType, float v) {
                        mMissionPreparationProgress = String.format("%s: %3.1f%%",djiProgressType,v*100);

                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callUpdateMissionState();
                            }
                        });
                    }
                }, new DJICommonCallbacks.DJICompletionCallback() {
                    @Override
                    public void onResult(DJIError djiError) {
                        mMissionPreparationProgress = "";
                        if( djiError == null) {
                            showToast("Mission prepared. Ready to start!", Toast.LENGTH_SHORT, "prepareMission()", MCLogger.LogType.i,true);
                        } else {
                            showToast(djiError.getDescription(), Toast.LENGTH_LONG, "prepareMission()", MCLogger.LogType.e,true);
                        }
                    }
                });
    }

    @Override
    public void startMissionExecution() {
            mDJIMissionManager.startMissionExecution(new DJICommonCallbacks.DJICompletionCallback() {
                @Override
                public void onResult(DJIError djiError) {
                    if(djiError == null) {
                        showToast("Mission started", Toast.LENGTH_SHORT, "startMissionExecution()", MCLogger.LogType.i,false);
                    } else {
                        showToast(djiError.getDescription(), Toast.LENGTH_LONG, "startMissionExecution()", MCLogger.LogType.e,true);
                    }
                }
            });
    }

    @Override
    public void pauseMissionExecution() {
        mDJIMissionManager.pauseMissionExecution(new DJICommonCallbacks.DJICompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                if(djiError == null) {
                    showToast("Mission paused",Toast.LENGTH_SHORT, "pauseMissionExecution()", MCLogger.LogType.i,true);
                } else {
                    showToast(djiError.getDescription(),Toast.LENGTH_LONG, "pauseMissionExecution()", MCLogger.LogType.e,true);
                }
            }
        });
    }

    @Override
    public void resumeMissionExecution() {
        mDJIMissionManager.resumeMissionExecution(new DJICommonCallbacks.DJICompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                if(djiError == null) {
                    showToast("Mission resumed",Toast.LENGTH_SHORT, "resumeMissionExecution()", MCLogger.LogType.i,false);
                } else {
                    showToast(djiError.getDescription(),Toast.LENGTH_LONG, "resumeMissionExecution()", MCLogger.LogType.e,true);
                }
            }
        });
    }



    @Override
    public void stopMissionExecution() {
        mDJIMissionManager.stopMissionExecution(new DJICommonCallbacks.DJICompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                if(djiError == null) {
                    showToast("Mission stopped",Toast.LENGTH_SHORT, "stopMissionExecution()", MCLogger.LogType.i,true);
                } else {
                    showToast(djiError.getDescription(),Toast.LENGTH_LONG, "stopMissionExecution()", MCLogger.LogType.e,true);
                }
            }
        });
    }



    @Override
    public void takeOff() {
        getFlightControler().takeOff(new DJICommonCallbacks.DJICompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                if(djiError == null) {
                    showToast("Has taken off",Toast.LENGTH_SHORT, "takeOff()", MCLogger.LogType.i,false);
                } else {
                    showToast(djiError.getDescription(),Toast.LENGTH_LONG, "takeOff()", MCLogger.LogType.e,true);
                }
            }
        });
    }

    @Override
    public void autoLand() {
        getFlightControler().autoLanding(new DJICommonCallbacks.DJICompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                if (djiError == null) {
                    showToast("Landing...", Toast.LENGTH_SHORT, "autoLand()", MCLogger.LogType.i,false);
                } else {
                    showToast(djiError.getDescription(), Toast.LENGTH_LONG, "autoLand()", MCLogger.LogType.e,true);
                }
            }
        });
    }

    @Override
    public void goHome(final float goHomeAltitude) {

        getFlightControler().setGoHomeAltitude(goHomeAltitude, new DJICommonCallbacks.DJICompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                if (djiError == null) {
                    showToast(String.format(Locale.GERMAN,"GoHomeAltitude set to %.2f",goHomeAltitude), Toast.LENGTH_SHORT, "setGoHomeAltitude()", MCLogger.LogType.i,false);

                    // schachteln, da sonst goHome vor setGoHomeAltitude aufgerufen wird.
                    getFlightControler().goHome(new DJICommonCallbacks.DJICompletionCallback() {
                        @Override
                        public void onResult(DJIError djiError) {
                            if (djiError == null) {
                                showToast("Going Home...", Toast.LENGTH_SHORT, "goHome()", MCLogger.LogType.i,String.format("GoHomeAltitude: %.2f",goHomeAltitude),false );
                            } else {
                                showToast(djiError.getDescription(), Toast.LENGTH_LONG, "goHome()", MCLogger.LogType.e,true);
                            }
                        }
                    });
                } else {
                    showToast(djiError.getDescription(), Toast.LENGTH_LONG, "setGoHomeAltitude()", MCLogger.LogType.e,true);
                }
            }
        });
    }

    @Override
    public void cancelAutoLanding() {
        getFlightControler().cancelAutoLanding(new DJICommonCallbacks.DJICompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                if (djiError == null) {
                    showToast("Landing Canceled", Toast.LENGTH_SHORT, "cancelAutoLanding()", MCLogger.LogType.i,true);
                } else {
                    showToast(djiError.getDescription(), Toast.LENGTH_LONG, "cancelAutoLanding()",  MCLogger.LogType.e,true);
                }
            }
        });
    }

    @Override
    public void cancelGoingHome() {
        getFlightControler().cancelGoHome(new DJICommonCallbacks.DJICompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                if (djiError == null) {
                    showToast("Going Home Canceled", Toast.LENGTH_SHORT, "cancelGoingHome()", MCLogger.LogType.i,true);
                } else {
                    showToast(djiError.getDescription(), Toast.LENGTH_LONG, "cancelGoingHome()", MCLogger.LogType.e,true);
                }
            }
        });
    }

    @Override
    public void cancelTakeOff() {
        getFlightControler().cancelTakeOff(new DJICommonCallbacks.DJICompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                if (djiError == null) {
                    showToast("TakeOff Canceled", Toast.LENGTH_SHORT, "cancelTakeOff()", MCLogger.LogType.i,true);
                } else {
                    showToast(djiError.getDescription(), Toast.LENGTH_LONG, "cancelTakeOff()",  MCLogger.LogType.e,true);
                }
            }
        });
    }


    @Override
    public void startSimulator(final DJISimulatorInitializationData initializationData) {

        getSimulator().startSimulator(initializationData, new DJICommonCallbacks.DJICompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                if(djiError == null) {
                    showToast("Simulator Started",Toast.LENGTH_SHORT, "StartSimulator()", MCLogger.LogType.i,
                            String.format("Lat[°]: %.7f, Lon[°]: %.7f, SatCount: %d.",initializationData.latitude,initializationData.longitude,initializationData.numOfSatellites),false);
                } else {
                    showToast(djiError.getDescription(),Toast.LENGTH_LONG, "StartSimulator()", MCLogger.LogType.e,true);
                    Log.e(LOG_TAG, djiError.getDescription());
                }
            }
        });
        // Set Homepoint (this should be called in the startSimulator callback, but that callback
        // gets never called when successful...) :
        final DJILocationCoordinate2D homepoint = new DJILocationCoordinate2D(initializationData.latitude, initializationData.longitude);

        getFlightControler().setHomeLocation(homepoint, new DJICommonCallbacks.DJICompletionCallback() {
            final String additionalData = String.format(Locale.US,"Lat[°]: %.7f, Lon[°]: %.7f",homepoint.getLatitude(),homepoint.getLongitude());
            @Override
            public void onResult(DJIError djiError) {
                if (djiError == null) {
                    showToast("Homepoint set to Simulation Start HomePoint", Toast.LENGTH_LONG, "StartSim-setHomeLocation()", MCLogger.LogType.i,additionalData,false);
                } else {
                    showToast(djiError.getDescription(), Toast.LENGTH_LONG, "StartSim-setHomeLocation()", MCLogger.LogType.e,additionalData,true);
                }

            }
        });
    }


    @Override
    public void stopSimulator() {
        // TODO: reset homepoint to current location:
        // that should be done in the callback function
        // stop sim
        getSimulator().stopSimulator(new DJICommonCallbacks.DJICompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                if(djiError == null) {
                    showToast("Simulation Stopped",Toast.LENGTH_SHORT, "StopSimulator()", MCLogger.LogType.i,false);
                } else {
                    showToast(djiError.getDescription(),Toast.LENGTH_LONG, "StopSimulator()", MCLogger.LogType.e,true);
                }
            }
        });
    }


    @Override
    public void setHomeLocationUsingAircraftCurrentLocation() {
        getFlightControler().setHomeLocationUsingAircraftCurrentLocation(new DJICommonCallbacks.DJICompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                if (djiError == null) {
                    final String additionalData = String.format("Lat[°]: %.7f, Lon[°]: %.7f.",getCurrentLocation().getLatitude(),getCurrentLocation().getLongitude());
                    showToast("HomePoint is set to current Aircraft Location", Toast.LENGTH_LONG, "setHomeLocationUsingAircraftCurrentLocation()", MCLogger.LogType.i,additionalData,true);
                } else {
                    showToast(djiError.getDescription(), Toast.LENGTH_LONG, "setHomeLocationUsingAircraftCurrentLocation()", MCLogger.LogType.e,true);
                    Log.e(LOG_TAG, djiError.getDescription());
                }
            }
        });
    }


    @Override
    public void setLastNotification(String lastNotification) {
        mLastNotification = lastNotification;
        if (mActivity != null)
            ((UpdateableActivityInterface) mActivity).setNotification(getLastNotification());
    }

    @Override
    public String getLastNotification() {
        return mLastNotification;
    }


    // Status Information (getter methods):
    @Override
    public String getMissionStateInfoText() {

        String missionState = "";
        if (getMissionPreparationProgress().equals("")) {
            if (getDJIMissionProgressStatus() != null) {
                if (getDJIMissionProgressStatus().getError() == null) {
                    if (getDJIMissionProgressStatus() instanceof DJIWaypointMission.DJIWaypointMissionStatus) {

                        DJIWaypointMission.DJIWaypointMissionExecutionState state = ((DJIWaypointMission.DJIWaypointMissionStatus) getDJIMissionProgressStatus()).getExecutionState();
                        int nextPoint = ((DJIWaypointMission.DJIWaypointMissionStatus) getDJIMissionProgressStatus()).getTargetWaypointIndex();
                        DJIMission mi = getCurrentExecutingMission();

                        if (mi instanceof DJIWaypointMission) {
                            int numPoints = ((DJIWaypointMission) mi).getWaypointCount() - 2;

                            if (nextPoint >= numPoints && state == DJIWaypointMission.DJIWaypointMissionExecutionState.Moving) {
                                missionState = "Coming Home";
                            } else {
                                if(state.toString().equals(DJIWaypointMission.DJIWaypointMissionExecutionState.Moving.toString())) {
                                    // nextPoint-1, so that NP: 1 is first "real" Waypoint.
                                    missionState = String.format(Locale.GERMAN, "%s to: %d (of %d)", state.toString(), nextPoint - 1,numPoints-2);
                                } else {
                                    // nextPoint-2, to be able to say "at" (current, not next Point):
                                    missionState = String.format(Locale.GERMAN, "%s at: %d (of %d)", state.toString(), nextPoint - 2,numPoints-2);
                                }
                            }
                        } else {
                            missionState = "CurrentMissionIsNoWaypointMission.";
                        }
                    } else {
                        missionState = "NoWaypointMissionExecuting.";
                    }
                } else {
                    missionState = getDJIMissionProgressStatus().getError().toString();
                }
            } else {
                missionState = "NoMissionProgressStatus";
            }
        } else{
            missionState = getMissionPreparationProgress();
        }
        return missionState;
    }

    @Override
    public DJIAircraft getAircraft() {
        return mDJIAircraft;
    }
    @Override
    public DJIFlightController getFlightControler() {
        return mDJIAircraft.getFlightController();
    }
    @Override
    public DJISimulator getSimulator() {
        return getFlightControler().getSimulator();
    }

    @Override
    public DJICamera getCamera() {
        return mDJIAircraft.getCamera();
    }
    @Override
    public void setDJICameraReceivedVideoDataCallback(DJICamera.CameraReceivedVideoDataCallback cameraReceivedVideoDataCallback) {
        mDJIAircraft.getCamera().setDJICameraReceivedVideoDataCallback(cameraReceivedVideoDataCallback);
    }

    @Override
    public DJIMission getCurrentExecutingMission() {
        return mDJIMissionManager.getCurrentExecutingMission();
    }
    @Override
    public DJIMission.DJIMissionProgressStatus getDJIMissionProgressStatus() {
        return mDJIMissionProgressStatus;
    }
    @Override
    public int getProductEnergyRemainingPercent(){
        return mProductEnergyRemainingPercent;
    }
    @Override
    public int getRemoteControllerEnergyRemainingPercent(){
        return mRemoteControllerEnergyRemainingPercent;
    }
    @Override
    public boolean isFlying() {
        return getFlightControler().getCurrentState().isFlying();
    }
    @Override
    public boolean hasSimulatorStarted() {
        // It can happen that the Simulator will be returned as null:
        if (getFlightControler().getSimulator() != null)
            return getFlightControler().getSimulator().hasSimulatorStarted();
        else
            return false;
    }
    @Override
    public String getMissionPreparationProgress() {
        return mMissionPreparationProgress;
    }
    @Override
    public DJILocationCoordinate3D getCurrentLocation() {
        return getFlightControler().getCurrentState().getAircraftLocation();
    }
    @Override
    public DJIGPSSignalStatus getGPSSignalStatus() {
        return getFlightControler().getCurrentState().getGpsSignalStatus();
    }
    @Override
    public double getAircraftHeading() {
        return getFlightControler().getCurrentState().getAttitude().yaw;
    }

    @Override
    public float getGimbalPitch() {
        return getAircraft().getGimbal().getAttitudeInDegrees().pitch;
    }

    @Override
    public boolean isGoingHome() {
        DJIFlightControllerFlightMode mode = getFlightControler().getCurrentState().getFlightMode();

        return mode == DJIFlightControllerFlightMode.GoHome;
    }

    @Override
    public boolean isLanding() {
        DJIFlightControllerFlightMode mode = getFlightControler().getCurrentState().getFlightMode();
        return mode == DJIFlightControllerFlightMode.AutoLanding;
    }

    @Override
    public boolean isTakingOff() {
        DJIFlightControllerFlightMode mode = getFlightControler().getCurrentState().getFlightMode();
        return mode == DJIFlightControllerFlightMode.AutoTakeOff;
    }


    // Using this helper method to show Toasts
    public void showToast(final String msg, int length, final String caller, final MCLogger.LogType logType, final Boolean showDialog) {
        if(length != Toast.LENGTH_LONG && length != Toast.LENGTH_SHORT) {
            length = Toast.LENGTH_SHORT;
        }
        final int lLength = length; // other thread => needs to be final;
        log(caller + ": " + msg,logType);
        try {
            mActivity.runOnUiThread(new Runnable() {
                @SuppressWarnings("WrongConstant")
                public void run() {
                    if ( showDialog) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                        // Add the buttons
                        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });
                        // Chain together various setter methods to set the dialog characteristics
                        builder.setMessage(msg).setTitle(mActivity.getString(R.string.notification));
                        // Create the AlertDialog
                        AlertDialog dialog = builder.create();
                        dialog.show();

                        // Play Notification sound:
                        SharedPreferences settings = mActivity.getSharedPreferences(SettingsListActivity.PREFS_NAME, 0);
                        if ( settings.getString(SettingsListActivity.PREF_PNS, "true").equals("true") ) {
                            try {
                                Uri notificationUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                                MediaPlayer mMediaPlayer = new MediaPlayer();
                                mMediaPlayer.setDataSource(mActivity, notificationUri);
                                mMediaPlayer.setLooping(false);
                                mMediaPlayer.prepare();
                                mMediaPlayer.start();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    } else {
                        Toast.makeText(mActivity, msg, lLength).show();
                    }
                    setLastNotification(msg);
                    Log.i(LOG_TAG, msg);
                }
            });
        } catch(NullPointerException e) {
            // do nothing, maybe activity has not been set, yet.
        }
    }

    // Using this helper method to show Toasts, invoked by other threads.
    public void showToast(final String msg, int length, final String caller, final MCLogger.LogType logType, final String additionalData, final Boolean showDialog) {
        if(length != Toast.LENGTH_LONG && length != Toast.LENGTH_SHORT) {
            length = Toast.LENGTH_SHORT;
        }
        final int lLength = length; // other thread => needs to be final;
        try {
            mActivity.runOnUiThread(new Runnable() {
            @SuppressWarnings("WrongConstant")
            public void run() {
                if ( showDialog) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                    // Add the buttons
                    builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
                    // Chain together various setter methods to set the dialog characteristics
                    builder.setMessage(msg).setTitle(mActivity.getString(R.string.notification));
                    // Create the AlertDialog
                    AlertDialog dialog = builder.create();
                    dialog.show();

                    // Play notification sound:
                    SharedPreferences settings = mActivity.getSharedPreferences(SettingsListActivity.PREFS_NAME, 0);
                    if ( settings.getString(SettingsListActivity.PREF_PNS, "true").equals("true") ) {
                        try {
                            Uri notificationUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                            MediaPlayer mMediaPlayer = new MediaPlayer();
                            mMediaPlayer.setDataSource(mActivity, notificationUri);
                            mMediaPlayer.setLooping(false);
                            mMediaPlayer.prepare();
                            mMediaPlayer.start();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                } else {
                    Toast.makeText(mActivity, msg, lLength).show();
                }
                setLastNotification(msg);
                log(caller + ": " + msg + ". " + additionalData, logType);
                Log.i(LOG_TAG,msg);
                }
            });
        } catch(NullPointerException e) {
            // do nothing, maybe activity has not been set, yet.
        }
    }




}
