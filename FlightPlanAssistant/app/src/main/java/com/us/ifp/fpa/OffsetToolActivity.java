package com.us.ifp.fpa;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.us.ifp.fpa.CrashReport.TopExceptionHandler;

import java.util.Locale;

import dji.common.error.DJIError;
import dji.common.util.DJICommonCallbacks;
import dji.sdk.flightcontroller.DJIFlightController;
import dji.sdk.products.DJIAircraft;

public class OffsetToolActivity extends AppCompatActivity{


    private final String LOG_TAG = OffsetToolActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offsettool);

        Thread.setDefaultUncaughtExceptionHandler(new TopExceptionHandler(this));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_offsettool, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == R.id.action_settings) {
            startActivity(new Intent(this,SettingsListActivity.class));
            return true;
        }

        if (id == R.id.action_about) {
            startActivity(new Intent(this,AboutActivity.class));
            return true;
        }


        return super.onOptionsItemSelected(item);
    }



    // Using this helper method to show Toasts, invoked by other threads.
    public void showToast(final String msg, int length) {
        if(length != Toast.LENGTH_LONG || length != Toast.LENGTH_SHORT) {
            length = Toast.LENGTH_SHORT;
        }
        final int lLength = length; // other thread => needs to be final;
        runOnUiThread(new Runnable() {
            @SuppressWarnings("WrongConstant")
            public void run() {
                Toast.makeText(getBaseContext(), msg, lLength).show();
                Log.i(LOG_TAG,msg);
            }
        });
    }


}
